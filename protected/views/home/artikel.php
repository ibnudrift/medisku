<section class="cover">
    <div class="row no-gutters">
    <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5>ARTIKEL</h5>
                    <p>My daughter has worn glasses since being 8 years old. She switched to contact lenses at 16. She is 21 now and told me yesterday that she is considering using her savings from her part-time job to get laser eye surgery.</p>
                    <!-- <button>SELENGKAPNYA</button> -->
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo $this->assetBaseurl ?>layanan1.jpg" alt="">
        </div>
    </div>
</section>

<section class="artikel-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-20">
                <div class="sidebar-art">
                    <h5>Artikel & Berita Terkait</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Terbaru</a></li>
                        <li><a href="#">Terpopuler</a></li>
                        <li><a href="#">Terlama</a></li>
                        <li><a href="#">Paling Banyak Dikunjungi</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-40">
                <div class="row list_artikel_blogs">
                    <?php for($i=1;$i<=6;$i++) { ?>
                        <div class="col-md-30">
                            <div class="box-content">
                                <div class="pictures">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/home/artikel_detail')); ?>">
                                        <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl ?>artikel1.jpg" alt="">
                                        </a>
                                </div>
                                <div class="info">
                                    <div class="py-2"></div>
                                    <span class="dates"><i class="fa fa-calendar"></i> &nbsp;<?php echo date('d M Y'); ?></span>
                                    <a href="<?php echo CHtml::normalizeUrl(array('/home/artikel_detail')); ?>">
                                        <h5>Understanding Drug And Alcohol Rehabilitation</h5>
                                    </a>
                                    <p>At every moment you can tell if the vibration that you are sending is either a positive one or a negative one by identifying the feeling you are experiencing.</p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="py-2"></div>
                <div class="blocks_def_pagination">
                    <nav aria-label="Page navigation example">
                      <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                      </ul>
                    </nav>
                    <div class="clear clearfix"></div>
                </div>
                <div class="py-1"></div>
            </div>
        </div>
    </div>
</section>

