<section class="cover">
    <div class="row no-gutters">
    <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['perawat_hero_title'] ?></h5>
                    <?php echo $this->setting['perawat_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['perawat_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="perawat-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Daftar Perawat Kami</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>
        
        <?php if (isset($_GET['Filter'])): ?>
        <div class="row justify-content-center my-5 text-center">
            <div class="col">
                <h4>Filter yang anda cari.</h4>
                <div class="py-1"></div>
                <p>Layanan: <?php echo Layanan::model()->findByPk($_GET['Filter']['tipe_perawat'])->titles; ?>, <?php echo $_GET['Filter']['jenis_kelamin'] ?></p>
            </div>
        </div>
        <?php endif ?>

        <?php if (is_array($model) && count($model) > 0): ?>
        <div class="row list_default_perawat">
            <?php foreach ($model as $key => $value): ?>
            <div class="col-md-15">
                <div class="box-content items">
                    <div class="pictures prelatife">
                        <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(270,258, '/images/intern_perawat/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">
                    </div>
                    <div class="contents">
                        <div class="row pb-1">
                            <div class="col">
                                <h5><?php echo $value->nama ?></h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p><?php echo Layanan::model()->findByPk($value->tipe_perawat_id)->titles; ?></p>
                            </div>
                        </div>
                        <div class="py-2"></div>
                        <a class="btn btn-info btnto_modDetail" href="#" data-toggle="modal" data-target="#perawat-modal_<?php echo $key ?>">DETAIL</a>
                    </div>

                </div>
                <!-- modal -->
                <div class="modal fade perawats_modal" id="perawat-modal_<?php echo $key ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-md modal-perawat" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-cover">
                                    <div class="modal-img-container">
                                        <img class="img-resposinve modal-photo" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(290,189, '/images/intern_perawat/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" style="width:100%">
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="modal-header-title clearfix" style="margin-top:90px">
                                    <h3 class="modal-name"><?php echo $value->nama; ?></h3>
                                </div>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="card-container top-up clearfix">
                                    <div class="details-info nopad blocks_nod_lar">
                                        <div class="row">
                                            <div class="col-md-30 nopad">
                                                <div class="logo-container">
                                                    <i class="fa modal-logo fa-venus"></i>
                                                </div>
                                                <span class="modal-gender"><?php echo $value->jenis_kelamin ?></span>
                                            </div>
                                            <div class="col-md-30 nopad">
                                                <div class="logo-container">
                                                    <i class="fa fa-heart"></i>
                                                </div>
                                                <span class="modal-religion"><?php echo $value->agama ?></span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-30 nopad">
                                                <div class="logo-container">
                                                    <i class="fa fa-user-md"></i>
                                                </div>
                                                <span class="modal-experience"><?php echo $value->pengalaman_lama ?> tahun pengalaman</span>
                                            </div>
                                            <div class="col-md-30 nopad">
                                                <div class="logo-container">
                                                    <i class="fa fa-graduation-cap"></i>
                                                </div>
                                                <span class="modal-education"><?php echo $value->pendidikan ?></span>
                                            </div>
                                        </div>

                                        
                                        <br>
                                        <div class="row">
                                            <div class="col-md-30 nopad">
                                                <b>Tinggi : </b><span class="modal-tinggi"><?php echo $value->tinggi_badan ?></span> Cm
                                            </div>
                                            <div class="col-md-30 nopad">

                                                <b>Berat :</b> <span class="modal-berat"><?php echo $value->berat_badan ?></span> Kg
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-60 nopad">
                                                <b>Sertifikat :</b><br> <p class="c"><span class="modal-sertifikasi"><?php echo nl2br($value->sertifikat) ?></span></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-60 nopad">
                                                    <b>Pengalaman :</b><br> <p class="c"><span class="modal-pengalaman"><?php echo nl2br($value->pengalaman) ?></span></p><p>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-60 nopad">
                                                <b>Penempatan :</b> <span class="modal-penempatan"><?php echo $value->penempatan ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row hide-be">
                                    <div class="col-md-60 text-center">
                                        <div class="py-2"></div>
                                        <a target="_blank" class="btn btn-dark" href="<?php echo Yii::app()->baseUrl.'/images/intern_perawat/'. $value->image ?>">
                                            Lihat Foto
                                        </a>
                                        <!-- <a href="<?php // echo CHtml::normalizeUrl(array('/home/perawat_order', 'perawat_id'=>$value->id, 'slug'=>Slug::Create($value->nama) )); ?>" class="btn btn-primary modal-action_url">
                                            Pesan
                                        </a> -->
                                        <a target="_blank" href="https://wa.me/<?php echo $this->setting['contact_wa_order'] ?>?text=<?php echo rawurlencode($this->setting['contact_wa_textorder']) ?>" class="btn btn-primary modal-action_url">
                                            Pesan
                                        </a>
                                        <div class="py-1"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end modal -->
            </div>
            <?php endforeach ?>
        </div>
        <?php else: ?>
            <div class="row justify-content-center my-5 text-center">
                <div class="col">
                    <h3>Maaf, data kosong!</h3>
                </div>
            </div>
        <?php endif ?>

        <div class="py-4"></div>
    </div>
</section>

<?php echo $this->renderPartial('//layouts/_layfoot_filter', array()); ?>
