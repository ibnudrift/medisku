<section class="cover">
    <div class="row no-gutters">
    <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['perawat_hero_title'] ?></h5>
                    <?php echo $this->setting['perawat_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['perawat_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="perawat-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Pesan Perawat</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>

        <!-- content section -->
        <div class="row content-text def_content">
            <div class="col-md-17">
                <div class="lefts_contact">
                <h4>Pesan Perawat.</h4>
                <p>Nama: <b><?php echo $data_par->nama; ?></b>
                   <br>
                </p>
                
                <div class="clear"></div>
                </div>

            </div>
            <div class="col-md-43">
                <div class="p-5 rights_contact">
                    <p>Please Fill this form</p>
                    
                    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                                      'type'=>'',
                                      'enableAjaxValidation'=>false,
                                      'clientOptions'=>array(
                                          'validateOnSubmit'=>false,
                                      ),
                                      'htmlOptions' => array(
                                          'enctype' => 'multipart/form-data',
                                      ),
                                  )); ?>
                   <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
                    <?php if(Yii::app()->user->hasFlash('success')): ?>
                        <?php $this->widget('bootstrap.widgets.TbAlert', array(
                            'alerts'=>array('success'),
                        )); ?>
                    <?php endif; ?>

                    <h4>DATA PEMESANAN</h4>
                    <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Nama Lengkap</label>
                            <?php echo $form->textField($model, 'nama_lengkap', array('class'=>'form-control', 'placeholder'=> '')); ?>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Email</label>
                            <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=> '')); ?>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">No Hp</label>
                            <?php echo $form->textField($model, 'no_hp', array('class'=>'form-control', 'placeholder'=> '')); ?>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Provinsi</label>
                            <?php echo $form->textField($model, 'provinsi', array('class'=>'form-control', 'placeholder'=> '')); ?>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Kabupaten</label>
                            <?php echo $form->textField($model, 'kabupaten', array('class'=>'form-control', 'placeholder'=> '')); ?>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Kecamatan</label>
                            <?php echo $form->textField($model, 'kecamatan', array('class'=>'form-control', 'placeholder'=> '')); ?>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Kelurahan</label>
                            <?php echo $form->textField($model, 'kelurahan', array('class'=>'form-control', 'placeholder'=> '')); ?>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Alamat</label>
                            <?php echo $form->textField($model, 'alamat_lengkap', array('class'=>'form-control', 'placeholder'=> '')); ?>
                          </div>
                        </div>
                    </div>

                    <h4>DATA PASIEN</h4>
                    <?php 
                    $frm_pasien = [
                                    [
                                        'keys'=>'nama_pasien',
                                        'names'=>'Nama Pasien',
                                    ],
                                    [
                                        'keys'=>'usia_pasien',
                                        'names'=>'Usia Pasien',
                                    ],
                                    [
                                        'keys'=>'tinggi_badan',
                                        'names'=>'Tinggi Badan',
                                    ],
                                    [
                                        'keys'=>'berat_badan',
                                        'names'=>'Berat Badan',
                                    ],
                                    [
                                        'keys'=>'kota_tinggal_pasien',
                                        'names'=>'Kota Tinggal Pasien',
                                    ],
                                    [
                                        'keys'=>'alat_yang_terpasang',
                                        'names'=>'Alat yang Terpasang',
                                    ],
                                    [
                                        'keys'=>'luka_pada_pasien',
                                        'names'=>'Luka pada Pasien',
                                    ],
                                    [
                                        'keys'=>'kondisi_pasien',
                                        'names'=>'Kondisi Pasien',
                                    ],                                    
                                  ];
                    $filters_frm = array_chunk($frm_pasien, 2);
                    ?>
                    <?php foreach ($filters_frm as $ke => $val): ?>
                    <div class="row">
                        <?php foreach ($val as $key => $value): ?>
                        <div class="col">
                          <div class="form-group">
                            <label for="input1"><?php echo $value['names']; ?></label>
                            <?php echo $form->textField($model, $value['keys'], array('class'=>'form-control')); ?>
                          </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                    <?php endforeach ?>

                    <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Asisten Rumah Tangga</label>
                            <div class="clear clearfix"></div>
                            <?php echo $form->RadioButtonList($model, 'asisten_rumah_tangga', ['1'=>'Ya','0'=>'Tidak'],array('class'=>'form-control')); ?>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="input1">Hewan Peliharaan</label>
                            <div class="clear clearfix"></div>
                            <?php echo $form->RadioButtonList($model, 'hewan_peliharaan', ['1'=>'Ya','0'=>'Tidak'],array('class'=>'form-control')); ?>
                          </div>
                        </div>
                    </div>

                      <div class="g-recaptcha" data-sitekey="6LfiJdcUAAAAAPc_3AiDzu79QR58UF270YH-EWB2"></div>
                      <br>
                      <button type="submit" class="btn btn-dark">SUBMIT</button>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        <!-- End content section -->
        <div class="py-1"></div>
    </div>
</section>

<?php echo $this->renderPartial('//layouts/_layfoot_filter', array()); ?>
<script src="https://www.google.com/recaptcha/api.js" ></script>
