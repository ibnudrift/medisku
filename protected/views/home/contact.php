<section class="cover">
    <div class="row no-gutters">
        <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['contact_hero_title'] ?></h5>
                    <?php echo $this->setting['contact_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['contact_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="layanan-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3><?php echo ucwords(strtolower($this->setting['contact_hero_title'])) ?></h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-60">
        		
        		<div class="row content-text def_content">
        			<div class="col-md-17">
        				<div class="lefts_contact">
        				<h4>Contact Office.</h4>
        				<p><?php echo $this->setting['contact_address_tx'] ?></p>
        				<p><b>View Map.</b><br>
        				<a target="_blank" href="<?php echo $this->setting['contact_maplink'] ?>">Click here to view map</a></p>


        				<p>
        					<b>Jam Buka Kantor.</b> <br>
        					<?php echo $this->setting['contact_hours_office'] ?>
        				</p>
						
        				<p>
        					<b>Email.</b> <br>
        					<a href="mailto:<?php echo $this->setting['contact_email'] ?>"><?php echo $this->setting['contact_email'] ?></a>
        				</p>
                        <p>
                            <b>Phone.</b> <br>
                            <?php echo $this->setting['contact_phone'] ?>
                        </p>
						
        				<p>
        					<b>Whatsapp.</b> <br>
        					<a href="https://wa.me/<?php echo $this->setting['contact_whatsapp'] ?>">+<?php echo $this->setting['contact_whatsapp'] ?></a>
        				</p>

        				<div class="clear"></div>
        				</div>

        			</div>
        			<div class="col-md-43">
        				<div class="p-5 rights_contact">
        					<p>Please Fill this form</p>
        					
        					<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                                              'type'=>'',
                                              'enableAjaxValidation'=>false,
                                              'clientOptions'=>array(
                                                  'validateOnSubmit'=>false,
                                              ),
                                              'htmlOptions' => array(
                                                  'enctype' => 'multipart/form-data',
                                              ),
                                          )); ?>
                               <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
                                <?php if(Yii::app()->user->hasFlash('success')): ?>
                                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                                        'alerts'=>array('success'),
                                        'fade'=>false,
                                    )); ?>
                                <?php endif; ?>

							  <div class="form-group">
							    <label for="input1">Name</label>
                                <?php echo $form->textField($model, 'name', array('class'=>'form-control', 'placeholder'=> '')); ?>
							  </div>

							  <div class="form-group">
							    <label for="input1">Email</label>
                                <?php echo $form->textField($model, 'email', array('class'=>'form-control', 'placeholder'=> '')); ?>
							  </div>
							  <div class="form-group">
							    <label for="input1">Subject</label>
                                <?php echo $form->textField($model, 'subject', array('class'=>'form-control', 'placeholder'=> '')); ?>
							  </div>
                              <div class="form-group">
                                <label for="input1">Company</label>
                                <?php echo $form->textField($model, 'company', array('class'=>'form-control', 'placeholder'=> '')); ?>
                              </div>
							  <div class="form-group">
							    <label for="input1">Message</label>
							    <?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'rows'=>2)); ?>
							  </div>

							  <div class="g-recaptcha" data-sitekey="6LfiJdcUAAAAAPc_3AiDzu79QR58UF270YH-EWB2"></div>
							  <br>
							  <button type="submit" class="btn btn-dark">SUBMIT</button>
							<?php $this->endWidget(); ?>

        				</div>
        			</div>
        		</div>
        		<div class="py-4"></div>
        		
        	</div>
        </div>
    </div>
</section>

<script src="https://www.google.com/recaptcha/api.js" ></script>