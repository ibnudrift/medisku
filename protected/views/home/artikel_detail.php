<section class="cover">
    <div class="row no-gutters">
    <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5>ARTIKEL</h5>
                    <p>My daughter has worn glasses since being 8 years old. She switched to contact lenses at 16. She is 21 now and told me yesterday that she is considering using her savings from her part-time job to get laser eye surgery.</p>
                    <!-- <button>SELENGKAPNYA</button> -->
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo $this->assetBaseurl ?>layanan1.jpg" alt="">
        </div>
    </div>
</section>

<section class="artikel-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-20">
                <div class="sidebar-art">
                    <h5>Artikel & Berita Terkait</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Terbaru</a></li>
                        <li><a href="#">Terpopuler</a></li>
                        <li><a href="#">Terlama</a></li>
                        <li><a href="#">Paling Banyak Dikunjungi</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-40">
                <div class="details_content_article">
                    <h3 class="customs_title titles">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum amet hic sed.</h3>
                    <div class="py-2"></div>
                    <span class="dates"><i class="fa fa-calendar"></i> &nbsp; <?php echo date('d M Y'); ?></span>
                    <div class="py-2"></div>
                    <div class="pictures"><img src="https://placehold.it/1080x650" alt="" class="img img-fluid"></div>
                    <div class="py-2"></div>
                    <div class="py-1"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, eos consectetur, corrupti quisquam totam et quibusdam voluptatum, quia maxime molestiae mollitia laudantium impedit obcaecati! Placeat alias, aspernatur libero ullam veritatis?</p>
                    <p>Earum eligendi perspiciatis, totam distinctio facere similique libero quaerat, beatae minima. Quo est nemo eum animi rem labore necessitatibus fuga dolorem voluptatibus inventore vitae quos impedit, repellat, ullam nulla, officia!</p>
                    <p>Quos accusantium autem ab nesciunt, doloribus, sequi esse culpa ratione minus possimus distinctio quam sit expedita! Fugit provident ducimus sed tenetur reiciendis numquam perferendis. Inventore, est. Adipisci inventore ipsa ipsam.</p>
                    <p>Nulla incidunt debitis illo possimus ex temporibus neque aspernatur ea laborum! In odit suscipit perferendis! Cupiditate inventore tenetur, aut vitae laborum tempora aliquam non quisquam quibusdam eligendi corporis hic minus.</p>

                    <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5e3fd008f14115f3"></script>
                    <!-- Go to www.addthis.com/dashboard to customize your tools --> 
                    <div class="addthis_inline_share_toolbox"></div>

                    <div class="clear clearfix"></div>
                    <div class="py-2"></div>
                </div>

                <div class="py-4"></div>
                <h3 class="customs_title">Artikel Lainnya</h3>
                <div class="py-3"></div>
                <div class="row list_artikel_blogs">
                    <?php for($i=1; $i<=2; $i++) { ?>
                        <div class="col-md-30">
                            <div class="box-content">
                                <div class="pictures">
                                    <img class="img img-fluid w-100" src="<?php echo $this->assetBaseurl ?>artikel1.jpg" alt="">
                                </div>
                                <div class="info">
                                    <div class="py-2"></div>
                                    <span class="dates"><i class="fa fa-calendar"></i> &nbsp;<?php echo date('d M Y'); ?></span>
                                    <h5>Understanding Drug And Alcohol Rehabilitation</h5>
                                    <p>At every moment you can tell if the vibration that you are sending is either a positive one or a negative one by identifying the feeling you are experiencing.</p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</section>

