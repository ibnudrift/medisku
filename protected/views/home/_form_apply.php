
  <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                  'type'=>'',
                  'enableAjaxValidation'=>false,
                  'clientOptions'=>array(
                      'validateOnSubmit'=>false,
                  ),
                  'htmlOptions' => array(
                      'enctype' => 'multipart/form-data',
                      // 'onsubmit' => 'alert("underconstruction"); reutrn false;',
                  ),
              )); ?>
   <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
    <?php if(Yii::app()->user->hasFlash('success')): ?>
        <?php $this->widget('bootstrap.widgets.TbAlert', array(
            'alerts'=>array('success'),
            'fade'=>false,
        )); ?>
    <?php endif; ?>
    <h5>Biodata</h5>
    <div class="row default">
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'nama',array('class'=>'form-control span5','maxlength'=>100, 'required'=>'required')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'email',array('class'=>'form-control span5','maxlength'=>100, 'required'=>'required')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'phone',array('class'=>'form-control span5','maxlength'=>100, 'required'=>'required')); ?>
      </div>
    </div>
    <div class="row default">
      <div class="col-60">
        <?php echo $form->dropDownListRow($model,'kelamin',  [ 'laki-laki'=>'Laki-Laki', 'perempuan'=>'Perempuan' ],array('class'=>'form-control span5', 'required'=>'required')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'form-control span5', 'required'=>'required')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'tanggal_lahir',array('class'=>'form-control datepic', 'required'=>'required')); ?>
      </div>
    </div>
    <div class="row default">
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'tinggi_badan',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'berat_badan',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'anak_ke',array('class'=>'form-control span5', 'placeholder'=>'2')); ?>
      </div>
    </div>
    <div class="row default">
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'anak_dari',array('class'=>'form-control span5', 'placeholder'=>'3')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'agama',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
    </div>
    <div class="py-2"></div>
    <h5>Lokasi</h5>
    <div class="row default">
      <div class="col-60">
        <?php echo $form->dropDownListRow($model, 'loc_provinsi',CHtml::listData(City::model()->findAll('1 GROUP BY province_id'), 'province_id', 'province'), array('class'=>'form-control', 'empty'=>'Pilih Provinsi')) ?>
      </div>
      <div class="col-60">
        <?php echo $form->dropDownListRow($model, 'loc_kota',array(), array('class'=>'form-control', 'empty'=>'Pilih Kota')) ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'loc_kecamatan',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'loc_kelurahan',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textAreaRow($model,'loc_alamat_lengkap',array('rows'=>3, 'class'=>'span8 form-control')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textAreaRow($model,'loc_alamat_domisili',array('rows'=>3, 'class'=>'span8 form-control')); ?>
      </div>
    </div>

    <div class="py-2"></div>
    <h5>Pendidikan Terakhir</h5>
    <div class="row default">
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'pendidikan_terakhir',array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'nama_sekolah',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'lulus_tahun',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
    </div>

    <div class="py-2"></div>
    <h5>Informasi Tambahan</h5>

    <div class="row default">
      <div class="col-60">
        <?php // echo $form->textFieldRow($model,'posisi_diminati',array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php // echo $form->textFieldRow($model,'keahlian',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
      <div class="col-60">
        <?php // echo $form->textFieldRow($model,'gaji_diinginkan',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
    </div>
   <div class="row default">
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'nama_orangtua',array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'kerja_orangtua',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'nama_suamiistri',array('class'=>'form-control span5','maxlength'=>100)); ?>
      </div>
    </div>
   <div class="row default">
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'pekerjaan_suamiistri',array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php 
        $mengetah = [
                    'iklan'=>'iklan',
                    'brosur'=>'brosur',
                    'teman'=>'teman',
                    'internet'=>'internet',
                    ];
        ?>
        <?php echo $form->dropDownListRow($model,'mengetahui_medisku', $mengetah,array('class'=>'form-control span5')); ?>
      </div>

      <div class="col-60">
        <?php echo $form->textAreaRow($model,'pelatihan',array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textAreaRow($model,'sertifikat',array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->dropDownListRow($model,'file_str', ['ya'=>'Ya', 'tidak'=>'Tidak'],array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textAreaRow($model,'pengalaman_kerja',array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'motivasi_bekerja',array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->dropDownListRow($model,'bersedia_kontraktahun', ['ya'=>'Ya', 'tidak'=>'Tidak'],array('class'=>'form-control span5')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->textFieldRow($model,'bersedia_standby',array('class'=>'form-control span5')); ?>
      </div>
    
    </div>

    <div class="py-2"></div>
    <h5>File - File</h5>
    <div class="row default">
      <div class="col-60">
        <?php echo $form->fileFieldRow($model,'file_foto',array('class'=>'', 'required'=>'required', 'accept'=>'image/jpeg')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->fileFieldRow($model,'file_ktp',array('class'=>'', 'required'=>'required', 'accept'=>'image/jpeg')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->fileFieldRow($model,'file_ijazah',array('class'=>'', 'required'=>'required', 'accept'=>'image/jpeg')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->fileFieldRow($model,'file_sertifikat',array('class'=>'', 'required'=>'required', 'accept'=>'image/jpeg')); ?>
      </div>
      <div class="col-60">
        <?php echo $form->fileFieldRow($model,'file_str',array('class'=>'', 'required'=>'required', 'accept'=>'image/jpeg')); ?>
      </div>
      
    </div>
    <div class="py-2"></div>



    <div class="g-recaptcha" data-sitekey="6LfiJdcUAAAAAPc_3AiDzu79QR58UF270YH-EWB2"></div>
    <br>
    <button type="submit" class="btn btn-light">SUBMIT</button>

  <?php $this->endWidget(); ?>


  <script type="text/javascript">

    // get district
    $('#TbMasterPerawat_loc_provinsi').change(function() {
        $.ajax({
            method: "GET",
            url: "<?php echo CHtml::normalizeUrl(array('/member/getkota')); ?>",
            data: { id: $('#TbMasterPerawat_loc_provinsi').val() }
        }).done(function(e) {
            $('#TbMasterPerawat_loc_kota').html(e);
        });     
    })
    
    $.ajax({
        method: "GET",
        url: "<?php echo CHtml::normalizeUrl(array('/member/getkota')); ?>",
        data: { id: $('#TbMasterPerawat_loc_provinsi').val() }
    }).done(function(e) {
        $('#TbMasterPerawat_loc_kota').html(e);
        $('#TbMasterPerawat_loc_kota').val('<?php echo $model->loc_kota ?>');
    });     

    $('#TbMasterPerawat_loc_kota').change(function() {
        $.ajax({
            method: "GET",
            url: "<?php echo CHtml::normalizeUrl(array('/member/getkecamatan')); ?>",
            data: { id: $('#TbMasterPerawat_loc_kota').val() }
        }).done(function(e) {
            $('#TbMasterPerawat_loc_kecamatan').html(e);
            $('#TbMasterPerawat_loc_kecamatan').html(e);
        });
        return false;
    });

    <?php if ($model->loc_kota): ?>
    // $.ajax({
    //         method: "GET",
    //         url: "<?php echo CHtml::normalizeUrl(array('/member/getkecamatan')); ?>",
    //         data: { id: <?php echo $model->city ?> }
    //     }).done(function(e) {
    //         $('#MeMember_district').html(e);
    //         $('#MeMember_district').val('<?php echo $model->district; ?>');
    // });
    <?php endif ?>
</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/theme.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
  $(function(){
    
    $('.datepic').datepicker({
      dateFormat: 'yy-mm-dd'
    });

  })
</script>