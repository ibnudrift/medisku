<section class="cover">
    <div class="row no-gutters">
        <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['layanan_hero_title'] ?></h5>
                    <?php echo $this->setting['layanan_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['layanan_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="layanan-sec-1">
    <div class="prelative container2">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Layanan Kami</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>

        <?php if (is_array($model) && count($model)): ?>
            <div class="row lists_layanan_defk">
                <?php foreach ($model as $key => $value): ?>
                    <div class="col-md-60">
                        <div class="box-content n_item">
                            <div class="row">
                                <div class="col-md-23">
                                    <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(360,185, '/images/layanan/'. $value->image, array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">
                                </div>
                                <div class="col-md-27 bx_content">
                                    <h5><?php echo strtoupper($value->titles) ?></h5>
                                    <p><?php echo substr($value->intro, 0, 120).'....' ?></p>
                                    <div class="py-1"></div>
                                    <div class="py-1"></div>
                                    <a href="<?php echo CHtml::normalizeUrl(array('/home/layanan_detail', 'id'=> $value->id, 'slug'=>Slug::Create($value->titles))); ?>" class="btn btn-info">LIHAT DETAIL</a>
                                </div>
                                <div class="col-md-10">
                                    <button onclick="window.open('<?php echo CHtml::normalizeUrl(array('/home/perawat')); ?>', '_SELF')">CARI PERAWAT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
            </div>
        <?php endif ?>

    </div>
</section>

<section class="layanan-sec-2">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Mengapa Memilih Kami</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>


        <?php 
        $notes_icon = [
                        [
                            'icons'=>'fa-heart',
                            'title'=>$this->setting['home4_itm_titles_1'],
                            'desc'=>$this->setting['home4_itm_contents_1'],
                        ],
                        [
                            'icons'=>'fa-refresh',
                            'title'=>$this->setting['home4_itm_titles_2'],
                            'desc'=>$this->setting['home4_itm_contents_2'],
                        ],
                        [
                            'icons'=>'fa-thumbs-up',
                            'title'=>$this->setting['home4_itm_titles_3'],
                            'desc'=>$this->setting['home4_itm_contents_3'],
                        ],
                        [
                            'icons'=>'fa-phone',
                            'title'=>$this->setting['home4_itm_titles_4'],
                            'desc'=>$this->setting['home4_itm_contents_4'],
                        ],                        
                        
                      ];
        ?>
        <div class="row no-gutters">
            <?php foreach ($notes_icon as $key => $value): ?>
                <div class="col-md-30">
                    <div class="box-content2-yeayyy">
                        <div class="row no-gutters">
                            <div class="col-md-10">
                                <i class="fa <?php echo $value['icons'] ?>" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-50">
                                <h6><?php echo $value['title'] ?></h6>
                                <p><?php echo $value['desc'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

    </div>
</section>


<?php echo $this->renderPartial('//layouts/_layfoot_filter', array()); ?>


<section class="home-sec-6 layanan-top d-none">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-20">
                <div class="title">
                    <h5>Artikel & Berita Terkait</h5>
                </div>
            </div>
            <div class="col-md-40">
                <!-- <div class="paginationclass">
                    <div class="rounded">
                        <a href="#">
                            <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="rounded">
                        <a href="#">
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div> -->
                <div class="paginationclass text-right">
                    <a class="btn btn-info" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Lihat lebih banyak</a>
                </div>
            </div>
        </div>

       <?php 
        $criteria=new CDbCriteria;
        $criteria->with = array('description');
        $criteria->order = 't.date_input DESC';

        $criteria->addCondition('t.active = "1"');
        $criteria->addCondition('description.language_id = :language_id');
        $criteria->params[':language_id'] = $this->languageID;
        $criteria->limit = 3;
        $blog = Blog::model()->findAll($criteria);
        ?>

        <div class="row">
        <?php foreach ($blog as $key => $value): ?>
            <div class="col-md-20">
                <div class="box-content">
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/index', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->title) )); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(338, 187, '/images/blog/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img img-fluid w-100" alt=""></a>
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/index', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->title) )); ?>"><h5><?php echo $value->description->title ?></h5></a>
                    <p><?php echo substr(strip_tags($value->description->content), 0, 105).'...'; ?></p>
                </div>
            </div>
        <?php endforeach ?>
        </div>

        <div class="py-5"></div>
    </div>
</section>