<section class="cover">
    <div class="row no-gutters">
        <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['lowongan_hero_title'] ?></h5>
                    <?php echo $this->setting['lowongan_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['lowongan_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="layanan-sec-1">
    <div class="prelative container2">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Lowongan</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>

        <div class="content-text def_content">
            <?php 
            $data_lowongan = [
                                [
                                    'pict'=>'https://insanmedika.co.id/assets/images/bayi.jpg',
                                    'titles'=>'PERAWAT DISABEL',
                                    'intro'=>'Kami membuka Lowongan Kerja Perawat Disabel untuk lulusan D3/D4 Kebidanan',
                                    'desc'=>'',
                                ],                         
                             ];
            ?>

            <div class="outers_list_lowongan_data">
                <?php if (is_array($model) && count($model) > 0): ?>
                    
                <div class="row justify-content-center">
                    <?php foreach ($model as $key => $value): ?>
                    <div class="col-md-20">
                        <div class="items">
                            <div class="pictures">
                                <a href="<?php echo CHtml::normalizeUrl(array('/home/lowongan_detail', 'id'=>$value->id, 'slug'=>Slug::Create($value->titles) )); ?>">
                                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(862,571, '/images/lowongan/'. $value->image, array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid">
                                </a>
                            </div>
                            <div class="infos">
                                <h4><?php echo $value->titles ?></h4>
                                <p><?php echo substr($value->intro, 0, 70).'....' ?></p>
                                <a href="<?php echo CHtml::normalizeUrl(array('/home/lowongan_detail', 'id'=>$value->id, 'slug'=>Slug::Create($value->titles) )); ?>" class="btn btn-info btn_def2">Detail</a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>                    
                </div>
                <?php else: ?>
                    <div class="text-center">
                    <h5>Not available at this moment</h5>
                    </div>
                <?php endif ?>
                <div class="py-4"></div>

                <div class="text-center"><a href="<?php echo CHtml::normalizeUrl(array('/home/applyregistration')); ?>" class="btn btn-info btn_def2 pl-5 pr-5">DAFTAR</a></div>
            </div>

            <div class="clear"></div>
        </div>
        <div class="py-5"></div>

    </div>
</section>

<?php echo $this->renderPartial('//layouts/_layfoot_filter', array()); ?>