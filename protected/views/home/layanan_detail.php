<section class="cover">
    <div class="row no-gutters">
        <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['layanan_hero_title'] ?></h5>
                    <?php echo $this->setting['layanan_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['layanan_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="layanan-sec-1">
    <div class="prelative container2">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Layanan Kami</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>
        <div class="row lists_layanan_defk">
            
                <div class="col-md-60">
                    <div class="box-content n_item">
                        <div class="row">
                            <div class="col-md-60 bx_content">
                                <div class="row">
                                    <div class="col-md-30">
                                        <h5><?php echo strtoupper($model->titles) ?></h5>
                                    </div>
                                    <div class="col-md-30 text-right">
                                        <a href="#" onclick="window.history.back();" class="btn btn-link">Kembali</a>
                                    </div>
                                </div>
                                <div class="py-3"></div>
                            </div>
                            <div class="col-md-60">
                                <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1080,552, '/images/layanan/'. $model->image, array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">
                            </div>
                            <div class="col-md-60">
                                <div class="bx_content details_fx_services">
                                    <?php echo $model->content ?>
                                </div>
                                <div class="py-3"></div>
                                <div class="text-center"><a target="_blank" href="https://wa.me/<?php echo $this->setting['contact_wa_order'] ?>" class="btn btn-info btn_def2 pl-5 pr-5">PESAN PERAWAT</a></div>
                            </div>
                        </div>
                    </div>
                </div>

        </div>

        <div class="row">
            <div class="col-md-60 text-center">
                <br>
            </div>
        </div>


    </div>
</section>

<section class="layanan-sec-2">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Mengapa Memilih Kami</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>


        <?php 
        $notes_icon = [
                        [
                            'icons'=>'fa-heart',
                            'title'=>$this->setting['home4_itm_titles_1'],
                            'desc'=>$this->setting['home4_itm_contents_1'],
                        ],
                        [
                            'icons'=>'fa-refresh',
                            'title'=>$this->setting['home4_itm_titles_2'],
                            'desc'=>$this->setting['home4_itm_contents_2'],
                        ],
                        [
                            'icons'=>'fa-thumbs-up',
                            'title'=>$this->setting['home4_itm_titles_3'],
                            'desc'=>$this->setting['home4_itm_contents_3'],
                        ],
                        [
                            'icons'=>'fa-phone',
                            'title'=>$this->setting['home4_itm_titles_4'],
                            'desc'=>$this->setting['home4_itm_contents_4'],
                        ],                        
                        
                      ];
        ?>
        <div class="row no-gutters">
            <?php foreach ($notes_icon as $key => $value): ?>
                <div class="col-md-30">
                    <div class="box-content2-yeayyy">
                        <div class="row no-gutters">
                            <div class="col-md-10">
                                <i class="fa <?php echo $value['icons'] ?>" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-50">
                                <h6><?php echo $value['title'] ?></h6>
                                <p><?php echo $value['desc'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

    </div>
</section>

<?php echo $this->renderPartial('//layouts/_layfoot_filter', array()); ?>

<section class="home-sec-6 layanan-top d-none"">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-20">
                <div class="title">
                    <h5>Artikel & Berita Terkait</h5>
                </div>
            </div>
            <div class="col-md-40">
                <div class="paginationclass">
                    <div class="rounded">
                        <a href="#">
                            <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="rounded">
                        <a href="#">
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php for($i=1;$i<=3;$i++) { ?>
                <div class="col-md-20">
                    <div class="box-content">
                        <img src="<?php echo $this->assetBaseurl ?>artikel1.jpg" alt="">
                        <h5>Understanding Drug And Alcohol Rehabilitation</h5>
                        <p>At every moment you can tell if the vibration that you are sending is either a positive one or a negative one by identifying the feeling you are experiencing.</p>
                    </div>
                </div>
                <?php } ?>
        </div>
    </div>
</section>