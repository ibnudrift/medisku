<section class="cover">
    <div class="row no-gutters">
        <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5>DAFTAR PERAWAT</h5>
                    <?php echo $this->setting['contact_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['contact_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="layanan-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Daftar Perawat</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-60">
        		
        		<div class="row content-text def_content">
        			<div class="col-md-17">
        				<div class="lefts_contact">
        				<h4>Info.</h4>
        				<p>Daftarkan diri anda menjadi tenaga perawat di Medisku.co.id, raihlah kesuksesan anda berasama kami.</p>
        				
        				<div class="clear"></div>
        				</div>

        			</div>
        			<div class="col-md-43">
        				<div class="p-5 rights_contact forms_register_apply">
        					<p>Please Fill this form</p>
        					
        					<?php echo $this->renderPartial('//home/_form_apply', array( 'model'=>$model )); ?>
        				</div>
        			</div>
        		</div>
        		<div class="py-4"></div>
        		
        	</div>
        </div>
    </div>
</section>

<script src="https://www.google.com/recaptcha/api.js" ></script>