<section class="cover">
    <div class="row no-gutters">
        <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['lowongan_hero_title'] ?></h5>
                    <?php echo $this->setting['lowongan_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['lowongan_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="layanan-sec-1">
    <div class="prelative container2">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Lowongan - <?php echo $model->titles ?></h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>

        <div class="content-text def_content">

            <div class="pictures text-center">
                <img class="img img-fluid d-block mx-auto" src="<?php echo Yii::app()->baseUrl.'/images/lowongan/'. $model->image ?>" alt="<?php echo $model->titles ?>">
            </div>
            <div class="py-3"></div>
            
            <?php echo $model->content ?>
            

            <div class="clear"></div>
        </div>
        <div class="py-5"></div>

    </div>
</section>

<?php echo $this->renderPartial('//layouts/_layfoot_filter', array()); ?>