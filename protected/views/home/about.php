<section class="cover">
    <div class="row no-gutters">
    <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['about_hero_title'] ?></h5>
                    <?php echo $this->setting['about_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['about_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="layanan-sec-1">
    <div class="prelative container2">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Tentang Kami</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>

        <div class="content-text def_content">
            <!-- <div class="pictures"><img src="https://placehold.it/1080x650" alt="" class="img img-fluid"></div> -->
            <?php echo $this->setting['about_content'] ?>

            <div class="py-4"></div>
            <div class="row block_vision">
                <div class="col-md-30">
                    <h3>VISI KAMI</h3>
                   <?php echo $this->setting['about_visi_content'] ?>
                </div>
                <div class="col-md-30">
                    <h3>MISI KAMI</h3>
                    <?php echo $this->setting['about_misi_content'] ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="py-5"></div>

    </div>
</section>

<?php echo $this->renderPartial('//layouts/_layfoot_filter', array()); ?>
