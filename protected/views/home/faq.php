<section class="cover">
    <div class="row no-gutters">
        <div class="col-md-30 order-2 order-sm-1">
            <div class="prelative container2">
                <div class="box-content">
                    <h5><?php echo $this->setting['faq_hero_title'] ?></h5>
                    <?php echo $this->setting['faq_hero_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30 order-1 order-sm-2">
            <img class="w-100 img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(709,450, '/images/static/'. $this->setting['faq_hero_cover'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
        </div>
    </div>
</section>

<section class="layanan-sec-1">
    <div class="prelative container2">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Faq</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>

        <?php 

        $criteria=new CDbCriteria;
        $criteria->with = array('description');
        $criteria->addCondition('description.language_id = :language_id');
        $criteria->params[':language_id'] = $this->languageID;
        $criteria->addCondition('status = 1');
        // $criteria->group = 't.id';

        $data = Faq::model()->findAll($criteria);
        ?>

        <div class="content-text def_content">
            <div id="accordion" class="customs_faq_block">
            <?php if (is_array($data) && count($data) > 0 ) : ?>
            <?php foreach ($data as $key => $value): ?>
              <div class="card">
                <div class="card-header" id="heading_<?php echo $key ?>">
                  <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?php echo $key ?>" aria-expanded="true" aria-controls="collapse_<?php echo $key ?>">
                      <?php echo ucwords($value->description->question) ?>
                    </button>
                  </h5>
                </div>
                <div id="collapse_<?php echo $key ?>" class="collapse <?php echo ($key == 0)? 'show':'' ?>" aria-labelledby="heading_<?php echo $key ?>" data-parent="#accordion">
                  <div class="card-body">
                    <?php echo $value->description->answer; ?>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
            <?php endif ?>

            </div>

            <div class="clear"></div>
        </div>
        <div class="py-5"></div>

    </div>
</section>


<section class="home-sec-4">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-20">
                <img src="<?php echo $this->assetBaseurl ?>pic-sec-4.png" alt="">
            </div>
            <div class="col-md-20">
                <div class="content">
                    <h5>Pilih Jenis Layanan yang Anda Butuhkan, lalu pilih Durasi Layanan</h5>
                    <p>My daughter has worn glasses since being 8 years old. She switched to contact lenses at 16. She is 21 now and told me yesterday that she is considering using her savings from her part-time job to get laser eye surgery.</p>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-content">
                    <div class="atas">
                        <i class="fa fa-medkit" aria-hidden="true"></i>
                        <p>Daftar Layanan</p>
                    </div>
                    <form class="form-inline">
                        <label class="" for="inlineFormCustomSelectPref">Pilih Jenis Layanan</label>
                        <select class="custom-select" id="inlineFormCustomSelectPref">
                            <option selected>— Jenis Layanan —</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                        <label class="label2" for="inlineFormCustomSelectPref">Pilih Durasi Layanan</label>
                        <select class="custom-select" id="inlineFormCustomSelectPref">
                            <option selected>— Jenis Layanan —</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                        <button type="submit" class="btn btn-primary">Cari Layanan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
