<section class="home-sec-1">
    <div class="prelative container2">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3><?php echo $this->setting['home1_title'] ?></h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-30">
                <?php /*<div class="row">
                    <div class="col-md-30 col-30">
                        <div class="image"><img class="img img-fluid" src="<?php echo $this->assetBaseurl ?>group.jpg" alt=""></div>
                    </div>
                    <div class="col-md-30 col-30">
                        <div class="image"><img class="img img-fluid" src="<?php echo $this->assetBaseurl ?>group2.jpg" alt=""></div>
                    </div>
                    <div class="col-md-30 col-30">
                        <div class="image atas"><img class="img img-fluid" src="<?php echo $this->assetBaseurl ?>group3.jpg" alt=""></div>
                    </div>
                    <div class="col-md-30 col-30">
                        <div class="image atas"><img class="img img-fluid" src="<?php echo $this->assetBaseurl ?>group4.jpg" alt=""></div>
                    </div>
                </div>*/ ?>
                <div class="row">
                    <div class="col-md-60">
                        <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['home1_picture'] ?>" alt="about banner">
                    </div>
                </div>
            </div>
            <div class="col-md-30">
                <div class="box-content">
                    <?php echo $this->setting['home1_content'] ?>
                    <div class="py-3"></div>
                    <a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>" class="btn btn-info btn_def2">Selengkapnya</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-sec-2">
    <div class="row no-gutters">
        <div class="col-md-30">
            <div class="prelative container2">
                <div class="box-content">
                    <?php echo $this->setting['home2_content'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-30">
            <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['home2_picture'] ?>" alt="about banner">

        </div>
    </div>
</section>

<section class="home-sec-3">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3><?php echo $this->setting['home3_titles'] ?></h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>
        <?php 
        
        $criteria=new CDbCriteria;
        // $criteria->with = array('description');
        // $criteria->addCondition('description.language_id = :language_id');
        // $criteria->params[':language_id'] = $this->languageID;
        // $criteria->group = 't.id';
        $criteria->addCondition('aktif = 1');
        $criteria->order = 't.tgl_input asc';
        $criteria->limit = 4;
        $model_layanan = Layanan::model()->findAll($criteria);
        ?>
        <div class="row">
            <?php foreach ($model_layanan as $key => $value): ?>
                <div class="col-md-15">
                    <div class="box-content">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/layanan_detail', 'id'=> $value->id, 'slug'=>Slug::Create($value->titles))); ?>"><img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(380,194, '/images/layanan/'. $value->image, array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt=""></a>
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/layanan_detail', 'id'=> $value->id, 'slug'=>Slug::Create($value->titles))); ?>"><h5><?php echo strtoupper($value->titles) ?></h5></a>
                        <p><?php echo substr($value->intro, 0, 70).'....' ?></p>
                        <div class="py-2 my-1"></div>
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/layanan_detail', 'id'=> $value->id, 'slug'=>Slug::Create($value->titles))); ?>" class="btn btn-info btn_def2">Detail Layanan</a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <?php 
        $notes_icon = [
                        [
                            'icons'=>'fa-heart',
                            'title'=>$this->setting['home4_itm_titles_1'],
                            'desc'=>$this->setting['home4_itm_contents_1'],
                        ],
                        [
                            'icons'=>'fa-refresh',
                            'title'=>$this->setting['home4_itm_titles_2'],
                            'desc'=>$this->setting['home4_itm_contents_2'],
                        ],
                        [
                            'icons'=>'fa-thumbs-up',
                            'title'=>$this->setting['home4_itm_titles_3'],
                            'desc'=>$this->setting['home4_itm_contents_3'],
                        ],
                        [
                            'icons'=>'fa-phone',
                            'title'=>$this->setting['home4_itm_titles_4'],
                            'desc'=>$this->setting['home4_itm_contents_4'],
                        ],                        
                        
                      ];
        ?>

        <div class="row no-gutters">
            <?php foreach ($notes_icon as $key => $value): ?>
                <div class="col-md-30">
                    <div class="box-content2-yeayyy">
                        <div class="row no-gutters">
                            <div class="col-md-10">
                                <i class="fa <?php echo $value['icons'] ?>" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-50">
                                <h6><?php echo $value['title'] ?></h6>
                                <p><?php echo $value['desc'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>

<?php echo $this->renderPartial('//layouts/_layfoot_filter', array()); ?>

<section class="home-sec-5">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="title-content">
                    <h3>Testimoni Pelanggan</h3>
                </div>
                <div class="arrow">
                    <img src="<?php echo $this->assetBaseurl ?>hr.svg" alt="">
                </div>
            </div>
        </div>
        <?php 
        $criteria=new CDbCriteria;
        $criteria->select = "t.*, pg_testimonial_description.content as contents";
        $criteria->join = "
        LEFT JOIN pg_testimonial_description ON pg_testimonial_description.testimonial_id=t.id
        ";
        $criteria->addCondition('pg_testimonial_description.language_id = :language_id');
        // $criteria->addCondition('t.`active` = 1');
        $criteria->params[':language_id'] = $this->languageID;
        $models_testimoni  = PgTestimonial::model()->findAll($criteria);
        ?>
        <div class="owl-carousel owl-theme outers_list_testimonial">
            <?php foreach ($models_testimoni as $key => $value): ?>
                <div class="item">
                    <div class="box-content">
                        <div class="pictures">
                            <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(130,130, '/images/testimonial/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">
                        </div>
                        <h6><?php echo $value->name ?></h6>
                        <p><?php echo $value->contents ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

        <div class="py-5 d-none d-sm-block"></div>
        <div class="py-3 d-block d-sm-none"></div>
        <div class="ht"></div>
        <div class="py-2"></div>

    </div>
</section>


<section class="home-sec-6">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-20">
                <div class="title">
                    <h5>Artikel & Berita Terkait</h5>
                </div>
            </div>
            <div class="col-md-40">
                <div class="paginationclass text-right">
                    <a class="btn btn-info" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Lihat lebih banyak</a>
                </div>
            </div>
        </div>
        
        <div class="py-2 my-1"></div>

        <?php 
        $criteria=new CDbCriteria;
        $criteria->with = array('description');
        $criteria->order = 't.date_input DESC';

        $criteria->addCondition('t.active = "1"');
        $criteria->addCondition('description.language_id = :language_id');
        $criteria->params[':language_id'] = $this->languageID;
        $criteria->limit = 3;
        $blog = Blog::model()->findAll($criteria);
        ?>

        <div class="row">
        <?php foreach ($blog as $key => $value): ?>
            <div class="col-md-20">
                <div class="box-content">
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->title) )); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(338, 187, '/images/blog/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" class="img img-fluid w-100" alt=""></a>
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id, 'slug'=>Slug::Create($value->description->title) )); ?>"><h5><?php echo $value->description->title ?></h5></a>
                    <p><?php echo substr(strip_tags($value->description->content), 0, 105).'...'; ?></p>
                </div>
            </div>
        <?php endforeach ?>
        </div>
    </div>
</section>
<div class="py-5 d-none d-sm-block"></div>
<div class="py-4 d-block d-sm-none"></div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:2,
                nav:false
            },
            1000:{
                items:3,
                nav:false,
                loop:false
            }
        }
    })
})
</script>