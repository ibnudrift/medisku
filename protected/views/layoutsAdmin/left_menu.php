<?php 
$session = new CHttpSession;
$session->open();
$checks_user = $session['login']['group_id'];
?>
<div class="leftmenu">        
    <ul class="nav nav-tabs nav-stacked">
        <li class="nav-header">Navigation</li>
        
        <li class="dropdown"><a href="#"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Home') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/home')); ?>">Statis Home</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/slide/index')); ?>">Slides Data</a></li>
            </ul>
        </li>
        
        <li class="dropdown"><a href="#"><span class="fa fa-adjust"></span> <?php echo Tt::t('admin', 'Layanan') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/layanan')); ?>">Statis Layanan</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/layanan/index')); ?>">Data Layanan</a></li>
            </ul>
        </li>
        
        <li class="dropdown"><a href="#"><span class="fa fa-umbrella"></span> <?php echo Tt::t('admin', 'Intern Perawat') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/perawat/index')); ?>">Perawat Overview</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/perawatCategory')); ?>">Kategori Perawat</a></li> -->
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/perawat')); ?>">Perawat Statis</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/perawatorder/index')); ?>">List Order Perawat</a></li> -->
            </ul>
        </li>

        <li class="dropdown"><a href="#"><span class="fa fa-paper-plane"></span> <?php echo Tt::t('admin', 'Lowongan / Perawat Register') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/lowongan/index')); ?>">Lowongan Overview</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/lowongan')); ?>">Lowongan Statis</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/masterPerawat/index')); ?>">Daftar Perawat</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-bullhorn"></span> <?php echo Tt::t('admin', 'Blog / Artikel') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/blog/index')); ?>">Data Artikel</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/categoryblog/index')); ?>">Category Artikel</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/artikel')); ?>">Statis Artikel</a></li>
            </ul>
        </li>
        <li>&nbsp;</li>
        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Testimonial') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/testimonial/index')); ?>">Testimonial Overview</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/faq')); ?>">FAQ Statis</a></li> -->
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-fax"></span> <?php echo Tt::t('admin', 'FAQ') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/faq/index')); ?>">FAQ Overview</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/faq')); ?>">FAQ Statis</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Static Other') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/about')); ?>">About</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/syarat')); ?>">Syarat Ketentuan</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/privasi')); ?>">Kebijakan Privasi</a></li>
            </ul>
        </li>

         
         <li class="dropdown"><a href="#"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Contact') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/static/contact')); ?>">Contact Statis</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/companyList/index')); ?>">List Company</a></li> -->
            </ul>
        </li>
        <li>&nbsp;</li>
        <li class="dropdown"><a href="#"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Newsletter') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/newslett')); ?>">Newsletter</a></li>
            </ul>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('setting/index')); ?>"><span class="fa fa-cogs"></span> <?php echo Tt::t('admin', 'General Setting') ?></a>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/SystemLogin/home/logout')); ?>"><span class="fa fa fa-sign-out"></span> Logout</a></li>
    </ul>
</div><!--leftmenu-->
