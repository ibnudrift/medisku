    <li class="nav-item <?php echo ($active_menu_pg == 'home/index')? 'active': '' ?>">
        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">BERANDA</a>
    </li>
    <li class="nav-item <?php echo ($active_menu_pg == 'home/layanan')? 'active': '' ?>">
        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/layanan')); ?>">LAYANAN</a>
    </li>
    <li class="nav-item <?php echo ($active_menu_pg == 'blog/index')? 'active': '' ?>">
        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">ARTIKEL</a>
    </li>
    <li class="nav-item <?php echo ($active_menu_pg == 'home/perawat')? 'active': '' ?>">
        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/perawat')); ?>">PERAWAT</a>
    </li>
    <li class="nav-item <?php echo ($active_menu_pg == 'home/lowongan')? 'active': '' ?>">
        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/lowongan')); ?>">LOWONGAN</a>
    </li>

    <li class="nav-item <?php echo ($active_menu_pg == 'home/contact')? 'active': '' ?>">
        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">KONTAK KAMI</a>
    </li>
    <li class="nav-item button-butt">
        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/applyregistration')); ?>">DAFTAR</a>
    </li>