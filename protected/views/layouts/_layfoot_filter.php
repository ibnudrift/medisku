<section class="home-sec-4">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-20">
                <img src="<?php echo $this->assetBaseurl ?>pic-sec-4.png" alt="">
            </div>
            <div class="col-md-20">
                <div class="content pt-2">
                    <?php echo $this->setting['home5_search_contents'] ?>
                </div>
            </div>
            <div class="col-md-20">
                <div class="box-content">
                    <div class="atas">
                        <i class="fa fa-medkit" aria-hidden="true"></i>
                        <p>Daftar Layanan</p>
                    </div>
                    <form class="form-inline" method="GET" action="<?php echo CHtml::normalizeUrl(array('/home/perawat')); ?>">
                        <label class="" for="inlineFormCustomSelectPref">Pilih Jenis Layanan</label>
                        <?php 
                        $all_layanan = Layanan::model()->findAll();
                        ?>
                        <select name="Filter[tipe_perawat]" class="custom-select" id="inlineFormCustomSelectPref" required>
                            <option value="">— Jenis Layanan —</option>
                            <?php foreach ($all_layanan as $key => $value): ?>
                                    <option value="<?php echo $value->id ?>"><?php echo ucwords($value->titles) ?></option>
                            <?php endforeach ?>  
                        </select>
                        <label class="label2" for="inlineFormCustomSelectPref">Pilih Jenis Kelamin</label>
                        <?php 
                        $d_kelamin = ['laki-laki'=>'Laki-Laki', 'perempuan'=>'Perempuan'];
                        ?>
                        <select name="Filter[jenis_kelamin]" class="custom-select" id="inlineFormCustomSelectPref" required>
                            <option value="">— Pilih jenis Kelamin —</option>
                            <?php foreach ($d_kelamin as $key => $value): ?>
                                <option value="<?php echo $value ?>"><?php echo ucwords($value) ?></option>
                            <?php endforeach ?>
                        </select>
                        <button type="submit" class="btn btn-primary">Cari Layanan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>