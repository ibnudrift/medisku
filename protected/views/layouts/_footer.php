<section class="footer">
  <div class="prelative container">
    <div class="row">
      <div class="col-md-60">
        <div class="logo_footers"><img class="img img-fluid" src="<?php echo $this->assetBaseurl ?>Medisku 2-02.png" alt=""></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-15">
        <div class="content">
          <h5>Alamat kantor</h5>   
          <ul class="sec-1 def_ul">
            <li><?php echo $this->setting['contact_address_tx'] ?></li>
            <li>&nbsp;</li>
            <li><i class="fa fa-phone"></i> <?php echo $this->setting['contact_phone'] ?></li>
            <li><a href="mailto:<?php echo $this->setting['contact_email'] ?>"><i class="fa fa-envelope"></i> <?php echo $this->setting['contact_email'] ?></a></li>
            <li><a href="https://wa.me/<?php echo $this->setting['contact_whatsapp'] ?>"><i class="fa fa-whatsapp"></i> Whatsapp</a></li>
          </ul>   
        </div>
      </div>
      <div class="col-md-15">
        <div class="content">
          <h5>HealthyCare</h5>  
          <ul class="sec-2 def_ul">
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Beranda</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/layanan')); ?>">Layanan</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Artikel</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/perawat')); ?>">Perawat</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/lowongan')); ?>">Lowongan</a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Kontak Kami</a></li>
            <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/home/syarat')); ?>">Syarat & Ketentuan</a></li>
            <li><a href="<?php // echo CHtml::normalizeUrl(array('/home/privasi')); ?>">Kebijakan Privasi</a></li> -->
            <li><a href="<?php echo CHtml::normalizeUrl(array('/home/faq')); ?>">FAQ</a></li>
          </ul>        
        </div>
      </div>
      <div class="col-md-13">
        <div class="content">
          <h5 class="medsos">Media Sosial</h5>
          <ul class="sec-3 def_ul">
            <?php if ($this->setting['social_facebook']): ?>
            <li><a target="_blank" href="<?php echo $this->setting['social_facebook'] ?>"><i class="fa fa-facebook-official" aria-hidden="true"></i><span>Facebook</span></a></li>
            <?php endif ?>
            <?php if ($this->setting['social_instagram']): ?>
            <li><a target="_blank" href="<?php echo $this->setting['social_instagram'] ?>"><i class="fa fa-instagram" aria-hidden="true"></i><span>Instagram</span></a></li>
            <?php endif ?>
            <?php if ($this->setting['social_twitter']): ?>
            <li><a target="_blank" href="<?php echo $this->setting['social_twitter'] ?>"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></a></li>
            <?php endif ?>
            <?php if ($this->setting['social_youtube']): ?>
            <li><a target="_blank" href="<?php echo $this->setting['social_youtube'] ?>"><i class="fa fa-youtube" aria-hidden="true"></i><span>YouTube</span></a></li>
            <?php endif ?>
          </ul>          
        </div>
      </div>
      <div class="col-md-17">
        <div class="content1">
          <h5><?php echo ucwords( strtolower('Daftar & Dapatkan UPDATE & TAWARAN MENARIK') ); ?></h5>
        <?php 
        $model22 = new NewsletterForm;
        if(isset($_POST['NewsletterForm']))
        {
          $model22->attributes=$_POST['NewsletterForm'];

          $status = true;

          if ($status AND $model22->validate()) {
            // $model22->save(false);

            // save data
            $new_mod = new TbNewsletter;
            $new_mod->nama = $model22->name;
            $new_mod->email = $model22->email;
            $new_mod->date_input = date("Y-m-d H:i:s");
            $new_mod->status = 1;
            $new_mod->save(false);

            Yii::app()->user->setFlash('success',Tt::t('front', 'Thank you for sending contacting form.'));
            $this->redirect(array('contact'));
          }
        }
        ?>

        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                        'type'=>'',
                        'enableAjaxValidation'=>false,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data',
                            // 'onsubmit' => 'alert("underconstruction"); reutrn false;',
                        ),
                    )); ?>
       <?php echo $form->errorSummary($model22, '', '', array('class'=>'alert alert-danger')); ?>
        <?php if(Yii::app()->user->hasFlash('success')): ?>
            <?php $this->widget('bootstrap.widgets.TbAlert', array(
                'alerts'=>array('success'),
                'fade'=>false,
            )); ?>
        <?php endif; ?>
             <div class="form-group">
              <?php echo $form->textField($model22,'email',array('class'=>'form-control', 'placeholder'=>'Masukkan Email.', 'required'=>'required')); ?>
            </div>
            <div class="form-group">
              <?php echo $form->textField($model22,'name',array('class'=>'form-control', 'placeholder'=>'Masukkan Nama.', 'required'=>'required')); ?>
            </div>
            <button>Daftar Sekarang</button>
        <?php $this->endWidget(); ?>

        </div>
      </div>
    </div>
  </div>
</section>

<div class="buttons_call_flying d-none">
    <a target="_blank" href="https://wa.me/<?php echo $this->setting['contact_wa_order'] ?>">
        <img src="<?php echo $this->assetBaseurl.'btns-wa.png'; ?>" alt="" class="img img-fluid">
    </a>
</div>

<div class="wptwa-container circled-handler-on-desktop circled-handler-on-mobile" data-delay-time="0" data-inactive-time="0" data-scroll-length="0" data-auto-display-on-mobile="off">
      <div class="wptwa-box">
        <div class="wptwa-wrapper">
          <div class="wptwa-description">
            <p><?php echo $this->setting['contact_wa_poopuptext'] ?></p>
          </div>
          
          <span class="wptwa-close"></span>
          <div class="wptwa-people">
            <a href="https://web.whatsapp.com/send?phone=<?php echo $this->setting['contact_wa_order'] ?>&amp;text=<?php echo rawurlencode($this->setting['contact_wa_textorder']) ?>" target="_blank" class="wptwa-account" data-number="<?php echo $this->setting['contact_wa_order'] ?>" data-auto-text="<?php echo $this->setting['contact_wa_textorder'] ?>">
              <div class="wptwa-face"><img src="https://ardanarotan.id/wp-content/uploads/2019/02/CSO-08-11-300x300.png" onerror="this.style.display='none'"></div>
              <div class="wptwa-info">
                <span class="wptwa-title">Customer Support</span>
                <span class="wptwa-name"><?php echo $this->setting['contact_wa_namacs'] ?></span>
              </div>
              <div class="wptwa-clearfix"></div>
            </a>
          </div>
        </div>
      </div>
      <span class="wptwa-handler">
        <i class="fa fa-whatsapp"></i>
        <span class="text">Hubungi Kami</span>      
      </span>
    </div>

<section class="float_foot d-block d-sm-none">
  <div class="row no-gutters">
    <div class="col">
      <div class="contents">
        <a target="_blank" href="https://wa.me/<?php echo $this->setting['contact_wa_order'] ?>&amp;text=<?php echo rawurlencode($this->setting['contact_wa_textorder']) ?>">
        <span class="c_icon">
          <img src="<?php echo $this->assetBaseurl ?>forma2-1-4.png" alt="" class="img img-fluid mx-auto" style="width: 28px; height: 28px; margin: -5px 5px 5px 0px;">
        </span>
        <span class="txts">Pesan Perawat</span>
        </a>
      </div>
    </div>
    <div class="col">
      <div class="contents">
        <a href="<?php echo CHtml::normalizeUrl(array('/home/applyregistration')); ?>">
        <span class="c_icon">
          <img src="<?php echo $this->assetBaseurl ?>forma2-1-8.png" alt="" class="img img-fluid mx-auto" style="width: 40px; height: 40px; margin: -10px 5px -4px 0px;">
        </span>
        <span class="txts">Daftar Perawat</span>
        </a>
      </div>
    </div>
    <div class="col">
      <div class="contents">
        <a target="_blank" href="https://tawk.to/chat/5e5bda02298c395d1cea8ebe/default">
        <span class="c_icon">
          <img src="<?php echo $this->assetBaseurl ?>cs.png" alt="" class="img img-fluid mx-auto" style="width: 30px; height: 30px;">
        </span>
        <span class="txts">Tanya Kami</span>
        </a>
      </div>
    </div>
    
  </div>
</section>