<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>
<?php // echo ($active_menu_pg == 'home/index')? 'homes_pg':'inside_pg'; ?>
<header class="heads homes_pg">
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top d-none d-sm-block">
        <div class="container">
            <div class="logos_nleft_header">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img class="img img-fluid" src="<?php echo $this->assetBaseurl ?>Medisku 2-02.png" alt=""></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item <?php echo ($active_menu_pg == 'home/index')? 'active': '' ?>">
                        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">BERANDA</a>
                    </li>
                    <?php 
                    $criteria=new CDbCriteria;
                    $criteria->addCondition('aktif = 1');
                    $criteria->order = 't.tgl_input asc';
                    $model_layanan = Layanan::model()->findAll($criteria);
                    ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          LAYANAN
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php foreach ($model_layanan as $key => $value): ?>
                                <a class="dropdown-item" href="<?php echo CHtml::normalizeUrl(array('/home/layanan_detail', 'id'=> $value->id, 'slug'=>Slug::Create($value->titles))); ?>"><?php echo $value->titles ?></a>
                            <?php endforeach ?>
                        </div>
                      </li>
                    <li class="nav-item <?php echo ($active_menu_pg == 'blog/index')? 'active': '' ?>">
                        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">ARTIKEL</a>
                    </li>
                    <li class="nav-item <?php echo ($active_menu_pg == 'home/perawat')? 'active': '' ?>">
                        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/perawat')); ?>">PERAWAT</a>
                    </li>
                    <li class="nav-item <?php echo ($active_menu_pg == 'home/lowongan')? 'active': '' ?>">
                        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/lowongan')); ?>">LOWONGAN</a>
                    </li>

                    <li class="nav-item <?php echo ($active_menu_pg == 'home/contact')? 'active': '' ?>">
                        <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">KONTAK KAMI</a>
                    </li>
                    <!-- <li class="nav-item button-butt">
                        <a class="nav-link" href="<?php // echo CHtml::normalizeUrl(array('/home/applyregistration')); ?>">DAFTAR</a>
                    </li> -->
                </ul>
            </div>
        </div>
    </nav>

    <div class="d-block d-sm-none">

        <nav class="white top_headmobile" role="navigation">
            <div class="nav-wrapper container">
                <a id="logo-container" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>" class="brand-logo">
                    <span class="logo_nmobile">
                    <img src="<?php echo $this->assetBaseurl ?>logo-medisku-mob.png" alt="" class="img img-fluid">
                    </span>
                </a>

              <ul id="nav-mobile" class="sidenav">
                <li class="tops_logo">
                    <span class="d-block mx-auto mw-160 py-2"><img src="<?php echo $this->assetBaseurl ?>Medisku 2-02.png" alt="" class="img img-fluid"></span>
                </li>
                <?php // echo $this->renderPartial('//layouts/_nav_top', array()); ?>

                <li class="nav-item <?php echo ($active_menu_pg == 'home/index')? 'active': '' ?>">
                    <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><i class="fa fa-home"></i>BERANDA</a>
                </li>
                <li class="nav-item <?php echo ($active_menu_pg == 'home/layanan')? 'active': '' ?>">
                    <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/layanan')); ?>"><i class="fa fa-briefcase"></i>LAYANAN</a>
                </li>
                <li class="nav-item <?php echo ($active_menu_pg == 'blog/index')? 'active': '' ?>">
                    <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>"><i class="fa fa-bullhorn"></i>ARTIKEL</a>
                </li>
                <li class="nav-item <?php echo ($active_menu_pg == 'home/perawat')? 'active': '' ?>">
                    <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/perawat')); ?>"><i class="fa fa-fire"></i>PERAWAT</a>
                </li>
                <li class="nav-item <?php echo ($active_menu_pg == 'home/lowongan')? 'active': '' ?>">
                    <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/lowongan')); ?>"><i class="fa fa-flag-o"></i>LOWONGAN</a>
                </li>

                <li class="nav-item <?php echo ($active_menu_pg == 'home/contact')? 'active': '' ?>">
                    <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>"><i class="fa fa-phone"></i>KONTAK KAMI</a>
                </li>
               <!--  <li class="nav-item button-butt">
                    <a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/applyregistration')); ?>"><i class="fa fa-gift"></i>DAFTAR</a>
                </li> -->
              </ul>
              
              <a href="#" data-target="nav-mobile" class="sidenav-trigger">
                <i class="fa fa-bars"></i>
              </a>
            </div>
          </nav>

    </div>

</header>
