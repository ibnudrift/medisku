<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
	<div class="block-wrap-fcss-top-conhome prelatife">
		<?php echo $this->renderPartial('//layouts/_header', array()); ?>

		<?php
		$criteria=new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->addCondition('active = 1');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->group = 't.id';
		$criteria->order = 't.id ASC';
		$slide = Slide::model()->with(array('description'))->findAll($criteria);
		?>
		
		<section>
			<div id="myCarousel_home" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
				<?php foreach ($slide as $key => $value): ?>
						<li data-target="#myCarousel_home" data-slide-to="<?php echo $key; ?>" <?php if($key == 0){ ?>class="active"<?php } ?> ></li>
					<?php endforeach; ?>
				</ol>
				<div class="carousel-inner" role="listbox">
				<?php foreach ($slide as $key => $value): ?>
                	<div class="carousel-item <?php echo ($key == 0)? 'active':''; ?>">
						<img class="w-100" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1600,863, '/images/slide/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="">
						<?php if ($value->hide_teks == 0): ?>
						<div class="carousel-caption d-none d-md-block">
							<h4><?php echo $value->description->title ?></h4> 
							<p><?php echo $value->description->content ?></p>
						</div>
						<?php endif ?>
					</div>
				<?php endforeach; ?>
				</div>
			</div>
		</section>

		
	<!-- end fcs -->
		<?php echo $content ?>
	<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
<?php $this->endContent(); ?>