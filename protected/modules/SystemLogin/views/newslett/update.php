<?php
$this->breadcrumbs=array(
	'Newsletter'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Newsletter',
	'subtitle'=>'Edit Newsletter',
);

$this->menu=array(
	array('label'=>'List Newsletter', 'icon'=>'th-list','url'=>array('index')),
	// array('label'=>'Add Newsletter', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Newsletter', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>