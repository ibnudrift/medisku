<?php
$this->breadcrumbs=array(
	'Tb Newsletters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TbNewsletter','url'=>array('index')),
	array('label'=>'Add TbNewsletter','url'=>array('create')),
);
?>

<h1>Manage Tb Newsletters</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'tb-newsletter-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'email',
		'date_input',
		'status',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
