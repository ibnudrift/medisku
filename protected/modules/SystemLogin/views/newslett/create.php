<?php
$this->breadcrumbs=array(
	'Newsletter'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Newsletter',
	'subtitle'=>'Add Newsletter',
);

$this->menu=array(
	array('label'=>'List Newsletter', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>