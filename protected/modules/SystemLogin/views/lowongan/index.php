<?php
$this->breadcrumbs=array(
	'Lowongan',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Lowongan',
	'subtitle'=>'Data Lowongan',
);

$this->menu=array(
	array('label'=>'Add Lowongan', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'Lowongan-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'titles',
		'intro',
		/*
		'custom_link',
		'content',
		'image',
		'aktif',
		'sortings',
		'tgl_input',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
