<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'layanan-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Lowongan</h4>
<div class="widgetcontent">
	<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>
	<?php $this->widget('ImperaviRedactorWidget', array(
	    'selector' => '.redactor',
	    'options' => array(
	        'imageUpload'=> $this->createUrl('admin/setting/uploadimage', array('type'=>'image')),
	        'clipboardUploadUrl'=> $this->createUrl('admin/setting/uploadimage', array('type'=>'clip')),
	    ),
	    'plugins' => array(
	        'clips' => array(
	        ),
	    ),
	)); ?>
	<?php // echo $form->textFieldRow($model,'tgl_input',array('class'=>'span5 datepicker')); ?>

	<?php echo $form->textFieldRow($model,'titles',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'intro',array('rows'=>3, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'content',array('rows'=>5, 'cols'=>50, 'class'=>'span8 redactor')); ?>

	<?php echo $form->fileFieldRow($model,'image',array(
	'hint'=>'<b>Note:</b> The image size is 768 x 515px. Larger images will be cropped automatically, please upload photos of horizontal size')); ?>
	<?php if ($model->scenario == 'update'): ?>
	<div class="control-group">
		<label class="control-label">&nbsp;</label>
		<div class="controls">
		<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(275,186, '/images/lowongan/'.$model->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
		</div>
	</div>
	<?php endif; ?>

	<?php // echo $form->textFieldRow($model,'custom_link',array('class'=>'span5','maxlength'=>225)); ?>

	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model, 'aktif', array(
        		'1'=>'Di Tampilkan',
        		'0'=>'Di Sembunyikan',
        	)); ?>
		</div>
		<div class="span6">
			<?php // echo $form->textFieldRow($model,'sortings',array('class'=>'span10')); ?>
		</div>
	</div>



		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
