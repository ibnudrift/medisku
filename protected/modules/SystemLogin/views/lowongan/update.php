<?php
$this->breadcrumbs=array(
	'Lowongan'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Lowongan',
	'subtitle'=>'Edit Lowongan',
);

$this->menu=array(
	array('label'=>'List Lowongan', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Lowongan', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>