<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titles')); ?>:</b>
	<?php echo CHtml::encode($data->titles); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('intro')); ?>:</b>
	<?php echo CHtml::encode($data->intro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custom_link')); ?>:</b>
	<?php echo CHtml::encode($data->custom_link); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aktif')); ?>:</b>
	<?php echo CHtml::encode($data->aktif); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sortings')); ?>:</b>
	<?php echo CHtml::encode($data->sortings); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_input')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_input); ?>
	<br />

	*/ ?>

</div>