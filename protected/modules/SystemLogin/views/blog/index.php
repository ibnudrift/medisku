<?php
$this->breadcrumbs=array(
	'Artikel',
);

$this->pageHeader=array(
	'icon'=>'fa fa-book',
	'title'=>'Artikel',
	'subtitle'=>'Data Artikel',
);

$this->menu=array(
	array('label'=>'Add Artikel', 'icon'=>'plus-sign','url'=>array('create')),
);
?>


<!-- <div class="widget">
	<div class="widgettitle">Filter Blog</div>
	<div class="widgetcontent"> -->
	<div class="row-fluid">
		<div class="span6">
		<label for="">&nbsp;</label>
		<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
		</div>
		<div class="span6">
			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'get',
				// 'htmlOptions'=>array('class'=>'hide hidden'),
			)); ?>
				<div class="row-fluid">
					<div class="span4">
						<label class="required" for="Blog_category_id">Category</label>
						<select id="Blog_category_id" name="Blog[topik_id]" class="input-block-level span12">
							<?php 
							$dataCategory = (PrdCategory::model()->categoryTree('category', $this->languageID));
							?>
							<option value="">---- Choose Category ----</option>
							<?php echo PrdCategory::model()->createOption($dataCategory) ?>				
						</select>
					</div>
					<div class="span4">
						<label for="">&nbsp;</label>
						<?php $this->widget('bootstrap.widgets.TbButton', array(
							'buttonType'=>'submit',
							'type'=>'primary',
							'label'=>'Search',
						)); ?>
						<?php $this->widget('bootstrap.widgets.TbButton', array(
							// 'buttonType'=>'button',
							'type'=>'primary',
							'label'=>'Reset',
							'url'=>Yii::app()->createUrl($this->route),
						)); ?>
					</div>
				</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
	<!-- </div>
</div> -->

<div class="row-fluid">
	<div class="span10">

	
		
		<?php $this->widget('bootstrap.widgets.TbGridView',array(
			'id'=>'promotion-grid',
			'dataProvider'=>$model->search($this->languageID),
			// 'filter'=>$model,
			'enableSorting'=>false,
			'summaryText'=>false,
			'type'=>'bordered',
			'columns'=>array(
				array(
		            'name'=>'title',
		        ),    
				// array(
		  //           'name'=>'writer_name',
		  //       ),    
				// array(
				// 	'name'=>'date_input',
				// 	'filter'=>false,
				// ),
				// array(
				// 	'name'=>'date_update',
				// 	'filter'=>false,
				// ),
				// array(
				// 	'name'=>'insert_by',
				// ),
				// array(
				// 	'name'=>'last_update_by',
				// ),
				array(
					'name'=>'active',
					'filter'=>array(
						'0'=>'Non Active',
						'1'=>'Active',
					),
					'type'=>'raw',
					'value'=>'($data->active == "1") ? "Di Tampilkan" : "Di Sembunyikan"',
				),
				array(
					'class'=>'bootstrap.widgets.TbButtonColumn',
					'template'=>'{update} {delete}'
				),
			),
		)); ?>
	</div>
	<?php /*
	<div class="span4">
		<?php $this->renderPartial('/categoryblog/category', array(
			'categoryModel'=>$categoryModel,
			'categoryModelDesc'=>$categoryModelDesc,
			'nestedCategory'=>$nestedCategory,
		)) ?>
	</div>*/ ?>

</div>
		