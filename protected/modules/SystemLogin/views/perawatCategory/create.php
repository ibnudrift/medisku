<?php
$this->breadcrumbs=array(
	'Perawat Category'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Perawat Category',
	'subtitle'=>'Add Perawat Category',
);

$this->menu=array(
	array('label'=>'List Perawat Category', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>