<?php
$this->breadcrumbs=array(
	'Perawat Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PerawatCategory','url'=>array('index')),
	array('label'=>'Add PerawatCategory','url'=>array('create')),
);
?>

<h1>Manage Perawat Categories</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'perawat-category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
