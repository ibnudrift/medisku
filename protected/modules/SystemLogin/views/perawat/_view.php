<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agama')); ?>:</b>
	<?php echo CHtml::encode($data->agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pendidikan')); ?>:</b>
	<?php echo CHtml::encode($data->pendidikan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tinggi_badan')); ?>:</b>
	<?php echo CHtml::encode($data->tinggi_badan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('berat_badan')); ?>:</b>
	<?php echo CHtml::encode($data->berat_badan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sertifikat')); ?>:</b>
	<?php echo CHtml::encode($data->sertifikat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pengalaman')); ?>:</b>
	<?php echo CHtml::encode($data->pengalaman); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penempatan')); ?>:</b>
	<?php echo CHtml::encode($data->penempatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('provinsi')); ?>:</b>
	<?php echo CHtml::encode($data->provinsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kota')); ?>:</b>
	<?php echo CHtml::encode($data->kota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_perawat_id')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_perawat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aktif')); ?>:</b>
	<?php echo CHtml::encode($data->aktif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_input')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_input); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pengalaman_lama')); ?>:</b>
	<?php echo CHtml::encode($data->pengalaman_lama); ?>
	<br />

	*/ ?>

</div>