<?php
$this->breadcrumbs=array(
	'Perawats'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Perawat', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Perawat', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit Perawat', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Perawat', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Perawat #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'image',
		'jenis_kelamin',
		'agama',
		'pendidikan',
		'tinggi_badan',
		'berat_badan',
		'sertifikat',
		'pengalaman',
		'penempatan',
		'provinsi',
		'kota',
		'tipe_perawat_id',
		'aktif',
		'tgl_input',
		'pengalaman_lama',
	),
)); ?>
