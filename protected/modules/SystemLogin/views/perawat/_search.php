<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'image',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'jenis_kelamin',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'agama',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'pendidikan',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'tinggi_badan',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'berat_badan',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textAreaRow($model,'sertifikat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'pengalaman',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'penempatan',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'provinsi',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'kota',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'tipe_perawat_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'aktif',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tgl_input',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pengalaman_lama',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
