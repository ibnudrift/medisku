<?php
$this->breadcrumbs=array(
	'Perawats'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Perawat','url'=>array('index')),
	array('label'=>'Add Perawat','url'=>array('create')),
);
?>

<h1>Manage Perawats</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'perawat-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'image',
		'jenis_kelamin',
		'agama',
		'pendidikan',
		/*
		'tinggi_badan',
		'berat_badan',
		'sertifikat',
		'pengalaman',
		'penempatan',
		'provinsi',
		'kota',
		'tipe_perawat_id',
		'aktif',
		'tgl_input',
		'pengalaman_lama',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
