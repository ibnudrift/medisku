<?php
$this->breadcrumbs=array(
	'Perawat'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Perawat',
	'subtitle'=>'Edit Perawat',
);

$this->menu=array(
	array('label'=>'List Perawat', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Perawat', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Data Layanan', 'icon'=>'table','url'=>array('/SystemLogin/layanan/index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>