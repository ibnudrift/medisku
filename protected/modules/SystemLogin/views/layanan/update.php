<?php
$this->breadcrumbs=array(
	'Layanan'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Layanan',
	'subtitle'=>'Edit Layanan',
);

$this->menu=array(
	array('label'=>'List Layanan', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Layanan', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>