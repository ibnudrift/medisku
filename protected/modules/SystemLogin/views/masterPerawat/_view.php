<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lahir); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tinggi_badan')); ?>:</b>
	<?php echo CHtml::encode($data->tinggi_badan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berat_badan')); ?>:</b>
	<?php echo CHtml::encode($data->berat_badan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anak_ke')); ?>:</b>
	<?php echo CHtml::encode($data->anak_ke); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anak_dari')); ?>:</b>
	<?php echo CHtml::encode($data->anak_dari); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agama')); ?>:</b>
	<?php echo CHtml::encode($data->agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('riwayat_sakit')); ?>:</b>
	<?php echo CHtml::encode($data->riwayat_sakit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loc_alamat_lengkap')); ?>:</b>
	<?php echo CHtml::encode($data->loc_alamat_lengkap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loc_provinsi')); ?>:</b>
	<?php echo CHtml::encode($data->loc_provinsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loc_kabupaten')); ?>:</b>
	<?php echo CHtml::encode($data->loc_kabupaten); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loc_kecamatan')); ?>:</b>
	<?php echo CHtml::encode($data->loc_kecamatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loc_kota')); ?>:</b>
	<?php echo CHtml::encode($data->loc_kota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loc_kelurahan')); ?>:</b>
	<?php echo CHtml::encode($data->loc_kelurahan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pendidikan_terakhir')); ?>:</b>
	<?php echo CHtml::encode($data->pendidikan_terakhir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_sekolah')); ?>:</b>
	<?php echo CHtml::encode($data->nama_sekolah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lulus_tahun')); ?>:</b>
	<?php echo CHtml::encode($data->lulus_tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('posisi_diminati')); ?>:</b>
	<?php echo CHtml::encode($data->posisi_diminati); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keahlian')); ?>:</b>
	<?php echo CHtml::encode($data->keahlian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pengalaman')); ?>:</b>
	<?php echo CHtml::encode($data->pengalaman); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaji_diinginkan')); ?>:</b>
	<?php echo CHtml::encode($data->gaji_diinginkan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_orangtua')); ?>:</b>
	<?php echo CHtml::encode($data->nama_orangtua); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kerja_orangtua')); ?>:</b>
	<?php echo CHtml::encode($data->kerja_orangtua); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_suamiistri')); ?>:</b>
	<?php echo CHtml::encode($data->nama_suamiistri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pekerjaan_suamiistri')); ?>:</b>
	<?php echo CHtml::encode($data->pekerjaan_suamiistri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mengetahui_medisku')); ?>:</b>
	<?php echo CHtml::encode($data->mengetahui_medisku); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foto_wajah')); ?>:</b>
	<?php echo CHtml::encode($data->foto_wajah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foto_full')); ?>:</b>
	<?php echo CHtml::encode($data->foto_full); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berkas_lamaran')); ?>:</b>
	<?php echo CHtml::encode($data->berkas_lamaran); ?>
	<br />

	*/ ?>

</div>