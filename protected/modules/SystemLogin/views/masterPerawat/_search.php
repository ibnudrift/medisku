<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'kelamin',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'tanggal_lahir',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tinggi_badan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'berat_badan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'anak_ke',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'anak_dari',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'agama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textAreaRow($model,'riwayat_sakit',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'loc_alamat_lengkap',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'loc_provinsi',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'loc_kabupaten',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'loc_kecamatan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'loc_kota',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'loc_kelurahan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'pendidikan_terakhir',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nama_sekolah',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'lulus_tahun',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'posisi_diminati',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'keahlian',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textAreaRow($model,'pengalaman',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'gaji_diinginkan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nama_orangtua',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'kerja_orangtua',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nama_suamiistri',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'pekerjaan_suamiistri',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textAreaRow($model,'mengetahui_medisku',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'foto_wajah',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'foto_full',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'berkas_lamaran',array('class'=>'span5','maxlength'=>225)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
