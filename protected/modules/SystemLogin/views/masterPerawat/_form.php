<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'tb-master-perawat-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data TbMasterPerawat</h4>
<div class="widgetcontent">
	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'kelamin',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'tanggal_lahir',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tinggi_badan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'berat_badan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'anak_ke',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'anak_dari',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'agama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textAreaRow($model,'riwayat_sakit',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'loc_alamat_lengkap',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
	<?php echo $form->textAreaRow($model,'loc_alamat_domisili',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
	
	<?php  
	$temp_prov = $model->loc_provinsi;
	$model->loc_provinsi = City::model()->findByPk($model->loc_kota)->province;
	?>
	<?php echo $form->textFieldRow($model,'loc_provinsi',array('class'=>'span5','maxlength'=>100)); ?>
	<?php 
	$model->loc_provinsi = $temp_prov;
	?>
	<?php echo $form->hiddenField($model,'loc_provinsi',array('class'=>'span5','maxlength'=>100)); ?>

	<?php  
	$temp_kota = $model->loc_kota;
	$model->loc_kota = City::model()->findByPk($model->loc_kota)->city_name;
	?>
	<?php echo $form->textFieldRow($model,'loc_kota',array('class'=>'span5','maxlength'=>100)); ?>
	<?php 
	$model->loc_kota = $temp_kota;
	?>
	<?php echo $form->hiddenField($model,'loc_kota',array('class'=>'span5','maxlength'=>100)); ?>


	<?php echo $form->textFieldRow($model,'loc_kota',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'loc_kecamatan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'loc_kelurahan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'pendidikan_terakhir',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nama_sekolah',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'lulus_tahun',array('class'=>'span5','maxlength'=>100)); ?>

	<?php // echo $form->textFieldRow($model,'posisi_diminati',array('class'=>'span5','maxlength'=>100)); ?>

	<?php // echo $form->textFieldRow($model,'keahlian',array('class'=>'span5','maxlength'=>100)); ?>

	<?php // echo $form->textAreaRow($model,'pengalaman',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php // echo $form->textFieldRow($model,'gaji_diinginkan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nama_orangtua',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'kerja_orangtua',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nama_suamiistri',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'pekerjaan_suamiistri',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textAreaRow($model,'mengetahui_medisku',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php // echo $form->textFieldRow($model,'foto_wajah',array('class'=>'span5','maxlength'=>225)); ?>

	<?php // echo $form->textFieldRow($model,'foto_full',array('class'=>'span5','maxlength'=>225)); ?>
	<?php // echo $form->textFieldRow($model,'berkas_lamaran',array('class'=>'span5','maxlength'=>225)); ?>

	<div class="control-group">
		<label class="control-label">Foto</label>
		<div class="controls">
			<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(150,76, '/images/mendaftar/'.$model->file_foto , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
			<div class="divider10"></div>
			<a download href="<?php echo Yii::app()->baseUrl.'/images/mendaftar/'. $model->file_foto ?>"><i class="fa fa-download"></i> Download File</a>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">KTP</label>
		<div class="controls">
			<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(150,76, '/images/mendaftar/'.$model->file_ktp , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
			<div class="divider10"></div>
			<a download href="<?php echo Yii::app()->baseUrl.'/images/mendaftar/'. $model->file_ktp ?>"><i class="fa fa-download"></i> Download File</a>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Ijazah</label>
		<div class="controls">
			<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(150,76, '/images/mendaftar/'.$model->file_ijazah , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
			<div class="divider10"></div>
			<a download href="<?php echo Yii::app()->baseUrl.'/images/mendaftar/'. $model->file_ijazah ?>"><i class="fa fa-download"></i> Download File</a>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Sertifikat</label>
		<div class="controls">
			<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(150,76, '/images/mendaftar/'.$model->file_sertifikat , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
			<div class="divider10"></div>
			<a download href="<?php echo Yii::app()->baseUrl.'/images/mendaftar/'. $model->file_sertifikat ?>"><i class="fa fa-download"></i> Download File</a>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Sertifikat STR</label>
		<div class="controls">
			<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(150,76, '/images/mendaftar/'.$model->file_str , array('method' => 'adaptiveResize', 'quality' => '90')) ?>"/>
			<div class="divider10"></div>
			<a download href="<?php echo Yii::app()->baseUrl.'/images/mendaftar/'. $model->file_str ?>"><i class="fa fa-download"></i> Download File</a>
		</div>
	</div>
	

		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
