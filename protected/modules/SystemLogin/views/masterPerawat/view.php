<?php
$this->breadcrumbs=array(
	'Tb Master Perawats'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TbMasterPerawat', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add TbMasterPerawat', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit TbMasterPerawat', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete TbMasterPerawat', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View TbMasterPerawat #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama',
		'email',
		'phone',
		'kelamin',
		'tempat_lahir',
		'tanggal_lahir',
		'tinggi_badan',
		'berat_badan',
		'anak_ke',
		'anak_dari',
		'agama',
		'riwayat_sakit',
		'loc_alamat_lengkap',
		'loc_provinsi',
		'loc_kabupaten',
		'loc_kecamatan',
		'loc_kota',
		'loc_kelurahan',
		'pendidikan_terakhir',
		'nama_sekolah',
		'lulus_tahun',
		'posisi_diminati',
		'keahlian',
		'pengalaman',
		'gaji_diinginkan',
		'nama_orangtua',
		'kerja_orangtua',
		'nama_suamiistri',
		'pekerjaan_suamiistri',
		'mengetahui_medisku',
		'foto_wajah',
		'foto_full',
		'berkas_lamaran',
	),
)); ?>
