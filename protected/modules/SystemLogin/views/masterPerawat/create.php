<?php
$this->breadcrumbs=array(
	'Master Perawat'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Master Perawat',
	'subtitle'=>'Add Master Perawat',
);

$this->menu=array(
	array('label'=>'List Master Perawat', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>