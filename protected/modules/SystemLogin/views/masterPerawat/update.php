<?php
$this->breadcrumbs=array(
	'Master Perawat'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Master Perawat',
	'subtitle'=>'Edit Master Perawat',
);

$this->menu=array(
	array('label'=>'List Master Perawat', 'icon'=>'th-list','url'=>array('index')),
	// array('label'=>'Add Master Perawat', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Master Perawat', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>