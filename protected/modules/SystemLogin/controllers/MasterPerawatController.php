<?php

class MasterPerawatController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('SystemLogin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TbMasterPerawat;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TbMasterPerawat']))
		{
			$model->attributes=$_POST['TbMasterPerawat'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("MasterPerawatController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TbMasterPerawat']))
		{
			$model->attributes=$_POST['TbMasterPerawat'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("MasterPerawatController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new TbMasterPerawat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TbMasterPerawat']))
			$model->attributes=$_GET['TbMasterPerawat'];

		$dataProvider=new CActiveDataProvider('TbMasterPerawat');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=TbMasterPerawat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tb-master-perawat-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionDownload_excel()
	{
		$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
		// $id = intval($_GET['id']);
		$data = TbMasterPerawat::model()->findAll();

		$filename = "master_perawat_".rand(10, 500).".xls";		 
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");

		$arr_nama = array(
						'nama',
						'email',
						'phone',
						'kelamin',
						'tempat_lahir',
						'tanggal_lahir',
						'tinggi_badan',
						'berat_badan',
						'anak_ke',
						'anak_dari',
						'agama',
						'riwayat_sakit',
						'alamat_lengkap',
						'loc_alamat_domisili',
						'provinsi',
						'kota',
						'kecamatan',
						'kelurahan',
						'pendidikan_terakhir',
						'nama_sekolah',
						'lulus_tahun',
						'posisi_diminati',
						'keahlian',
						'pengalaman',
						'gaji_diinginkan',
						'nama_orangtua',
						'kerja_orangtua',
						'nama_suamiistri',
						'pekerjaan_suamiistri',
						'mengetahui_medisku',
						'pelatihan',
						'sertifikat',
						'file_str',
						'pengalaman_kerja',
						'motivasi_bekerja',
						'bersedia_kontrak_tahun',
						'bersedia_standby',
						);


		$str = '<table border="1"><tr>';

		foreach ($arr_nama as $key => $value) {
			$str .=	'<td>'. ucwords( str_replace('_', ' ', $value) ) .'</td>';
		}
		$str .= '</tr>';

		foreach ($data as $key => $value) {
			$prov_name = City::model()->find('t.province_id = :prov_id', array(':prov_id'=> $value->loc_provinsi) );
			$city_data = City::model()->findByPk($value->loc_kota);

			$str .= '<tr>';
			$str .= '<td>'. $value->nama .'</td>';
			$str .= '<td>'. $value->email .'</td>';
			$str .= '<td>'. $value->phone .'</td>';
			$str .= '<td>'. $value->kelamin .'</td>';
			$str .= '<td>'. $value->tempat_lahir .'</td>';
			$str .= '<td>'. $value->tanggal_lahir .'</td>';
			$str .= '<td>'. $value->tinggi_badan .'</td>';
			$str .= '<td>'. $value->berat_badan .'</td>';
			$str .= '<td>'. $value->anak_ke .'</td>';
			$str .= '<td>'. $value->anak_dari .'</td>';
			$str .= '<td>'. $value->agama .'</td>';
			$str .= '<td>'. $value->riwayat_sakit .'</td>';
			$str .= '<td>'. $value->loc_alamat_lengkap .'</td>';
			$str .= '<td>'. $value->loc_alamat_domisili .'</td>';
			$str .= '<td>'. $prov_name->province .'</td>';
			$str .= '<td>'. $city_data->city_name .'</td>';
			$str .= '<td>'. $value->loc_kecamatan .'</td>';
			$str .= '<td>'. $value->loc_kelurahan .'</td>';
			$str .= '<td>'. $value->pendidikan_terakhir .'</td>';
			$str .= '<td>'. $value->nama_sekolah .'</td>';
			$str .= '<td>'. $value->lulus_tahun .'</td>';
			$str .= '<td>'. $value->posisi_diminati .'</td>';
			$str .= '<td>'. $value->keahlian .'</td>';
			$str .= '<td>'. $value->pengalaman .'</td>';
			$str .= '<td>'. $value->gaji_diinginkan .'</td>';
			$str .= '<td>'. $value->nama_orangtua .'</td>';
			$str .= '<td>'. $value->kerja_orangtua .'</td>';
			$str .= '<td>'. $value->nama_suamiistri .'</td>';
			$str .= '<td>'. $value->pekerjaan_suamiistri .'</td>';
			$str .= '<td>'. $value->mengetahui_medisku .'</td>';
			$str .= '<td>'. $value->pelatihan .'</td>';
			$str .= '<td>'. $value->sertifikat .'</td>';
			$str .= '<td>'. $value->file_str .'</td>';
			$str .= '<td>'. $value->pengalaman_kerja .'</td>';
			$str .= '<td>'. $value->motivasi_bekerja .'</td>';
			$str .= '<td>'. $value->bersedia_kontraktahun .'</td>';
			if ($value->bersedia_standby != 0) {
				$str .= '<td>Ya</td>';
			} else {
				$str .= '<td>Tidak</td>';
			}
			$str .= '</tr>';
		}
		$str .= '</table>';

		echo $str;
		Yii::app()->end();

	}


}
