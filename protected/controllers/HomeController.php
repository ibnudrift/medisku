<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionTestmail()
	{
		// test email
		$this->layout = '//layouts/_blank';

		$model = OrOrder::model()->findByPk(12);
		// get or_order testing
		$order = OrOrderProduct::model()->findAll('order_id = :order_id', array(':order_id'=>$model->id));
	    $bank = Bank::model()->findAll();
	    
		$mail = $this->renderPartial('//mail/cart2', array(
			'bank'=>$bank,
			'data' => $order,
			'modelOrder' => $model,
		), true);
		
		echo $mail;
		exit;
	}

	public function actionDummy()
	{
		Dummy::createDummyProduct();
		echo '<META http-equiv="refresh" content="0;URL=http://localhost/dv-computers/home/dummy">';
	}
	public function actionCroncategory()
	{
		$data = PrdProduct::model()->findAll();
		foreach ($data as $key => $value) {
			$tag = PrdCategory::model()->getBreadcrump($value->category_id, $this->languageID);
			$dataTag = array();
			foreach ($tag as $k => $v) {
				$dataTag[] = $k;
			}
			$value->tag = implode(', ', $dataTag);
			$value->save();
		}
	}

	public function actionIndex()
	{
		// $criteria = new CDbCriteria;
		// $criteria->with = array('description');
		// $criteria->addCondition('status = "1"');
		// $criteria->addCondition('description.language_id = :language_id');
		// $criteria->params[':language_id'] = $this->languageID;
		// $criteria->order = 'date_input DESC';
		// $_GET['category'] = 0;
		// if ($_GET['category'] != '') {
		// 	$criteria->addCondition('t.category_id = :category');
		// 	$criteria->params[':category'] = $_GET['category'];
		// }
		// $product = new CActiveDataProvider('PrdProduct', array(
		// 	'criteria'=>$criteria,
		//     'pagination'=>array(
		//         'pageSize'=>8,
		//     ),
		// ));

		$this->pageTitle = ($this->setting['home_meta_title'] != '')? $this->setting['home_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['home_meta_keyword'] != '')? $this->setting['home_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['home_meta_description'] != '')? $this->setting['home_meta_description'] : $this->metaDesc;

		$this->layout='//layouts/column1';
		$this->render('index', array(
			// 'product'=>$product,
		));
	}

	public function actionAbout()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'About - '.$this->pageTitle;

		$this->pageTitle = ($this->setting['about_meta_title'] != '')? $this->setting['about_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['about_meta_keyword'] != '')? $this->setting['about_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['about_meta_description'] != '')? $this->setting['about_meta_description'] : $this->metaDesc;

		$this->render('about', array(
		));
	}

	public function actionProducts()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Product & Accessories - '.$this->pageTitle;

		$criteria = new CDbCriteria;
		$criteria->with = array('description', 'alternateImage');
		$criteria->order = 'date ASC';
		$criteria->addCondition('t.status = "1"');
		$data = PrdProduct::model()->findAll($criteria);

		$this->pageTitle = ($this->setting['product_meta_title'] != '')? $this->setting['product_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['product_meta_keyword'] != '')? $this->setting['product_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['product_meta_description'] != '')? $this->setting['product_meta_description'] : $this->metaDesc;

		$this->render('products', array(
			'data' => $data,
		));
	}

	public function actionApplication()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Applications - '.$this->pageTitle;

		$this->pageTitle = ($this->setting['application_meta_title'] != '')? $this->setting['application_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['application_meta_keyword'] != '')? $this->setting['application_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['application_meta_description'] != '')? $this->setting['application_meta_description'] : $this->metaDesc;

		$this->render('application', array(
			'data' => $data,
		));
	}

	public function actionInstallation()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Installation - '.$this->pageTitle;

		// $criteria = new CDbCriteria;
		// $criteria->order = 'sorting ASC';
		// $data = Sertifikat::model()->findAll($criteria);

		$this->pageTitle = ($this->setting['install_meta_title'] != '')? $this->setting['install_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['install_meta_keyword'] != '')? $this->setting['install_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['install_meta_description'] != '')? $this->setting['install_meta_description'] : $this->metaDesc;

		$this->render('installation', array(
			// 'data' => $data,
		));
	}

	public function actionLayanan()
	{
		$this->pageTitle = 'Layanan - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['layanan_meta_title'] != '')? $this->setting['layanan_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['layanan_meta_keyword'] != '')? $this->setting['layanan_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['layanan_meta_description'] != '')? $this->setting['layanan_meta_description'] : $this->metaDesc;

        // $criteria->with = array('description');
        // $criteria->addCondition('description.language_id = :language_id');
        // $criteria->params[':language_id'] = $this->languageID;
        // $criteria->group = 't.id';
		$criteria=new CDbCriteria;
        $criteria->addCondition('aktif = 1');
        $criteria->order = 't.tgl_input asc';
        $model = Layanan::model()->findAll($criteria);

		$this->render('layanan', array(
			'model'=>$model,	
		));
	}

	public function actionLayanan_detail()
	{
		$this->pageTitle = 'Layanan Detail - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['layanan_meta_title'] != '')? $this->setting['layanan_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['layanan_meta_keyword'] != '')? $this->setting['layanan_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['layanan_meta_description'] != '')? $this->setting['layanan_meta_description'] : $this->metaDesc;

		$criteria=new CDbCriteria;
        // $criteria->with = array('description');
        // $criteria->addCondition('description.language_id = :language_id');
        // $criteria->params[':language_id'] = $this->languageID;
        // $criteria->group = 't.id';
        $criteria->addCondition('t.id = :ids');
        $criteria->params[':ids'] = intval($_GET['id']);
        $criteria->addCondition('aktif = 1');
        $criteria->order = 't.tgl_input asc';
        $model = Layanan::model()->find($criteria);

		$this->render('layanan_detail', array(
			'model'=>$model,	
		));
	}

	public function actionPerawat()
	{
		$this->pageTitle = 'Perawat - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['install_meta_title'] != '')? $this->setting['install_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['install_meta_keyword'] != '')? $this->setting['install_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['install_meta_description'] != '')? $this->setting['install_meta_description'] : $this->metaDesc;

		$criteria=new CDbCriteria;
        $criteria->addCondition('t.aktif = 1');
        $criteria->order = 't.tgl_input desc';
        if (isset($_GET['Filter'])) {
	        $criteria->addCondition('t.jenis_kelamin = :jen_kel');
	        $criteria->params[':jen_kel'] = htmlspecialchars($_GET['Filter']['jenis_kelamin']);

	        $criteria->addCondition('(t.tipe_perawat_id LIKE :q)');
            $criteria->params[':q'] = '%\"'.$_GET['Filter']['tipe_perawat'].'\"%';
        }

		$model = Perawat::model()->findAll($criteria);

		$this->render('perawat', array(
			'model'=>$model,	
		));
	}

	public function actionPerawat_detail()
	{
		$this->pageTitle = 'Perawat - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['install_meta_title'] != '')? $this->setting['install_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['install_meta_keyword'] != '')? $this->setting['install_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['install_meta_description'] != '')? $this->setting['install_meta_description'] : $this->metaDesc;

		$this->render('perawat_detail', array(
			'model'=>$model,	
		));
	}

	public function actionArtikel()
	{

		$this->pageTitle = ($this->setting['install_meta_title'] != '')? $this->setting['install_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['install_meta_keyword'] != '')? $this->setting['install_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['install_meta_description'] != '')? $this->setting['install_meta_description'] : $this->metaDesc;


		$this->pageTitle = 'Artikel - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('artikel', array(
			'model'=>$model,	
		));
	}

	public function actionArtikel_detail()
	{

		$this->pageTitle = ($this->setting['install_meta_title'] != '')? $this->setting['install_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['install_meta_keyword'] != '')? $this->setting['install_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['install_meta_description'] != '')? $this->setting['install_meta_description'] : $this->metaDesc;


		$this->pageTitle = 'Artikel - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('artikel_detail', array(
			'model'=>$model,	
		));
	}

	public function actionSyarat()
	{
		$this->pageTitle = 'Syarat - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['syarat_meta_title'] != '')? $this->setting['syarat_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['syarat_meta_keyword'] != '')? $this->setting['syarat_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['syarat_meta_description'] != '')? $this->setting['syarat_meta_description'] : $this->metaDesc;

		$this->render('syarat', array(
			'model'=>$model,	
		));
	}

	public function actionPrivasi()
	{
		$this->pageTitle = 'Kebijakan Privasi - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['privasi_meta_title'] != '')? $this->setting['privasi_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['privasi_meta_keyword'] != '')? $this->setting['privasi_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['privasi_meta_description'] != '')? $this->setting['privasi_meta_description'] : $this->metaDesc;

		$this->render('privasi', array(
			'model'=>$model,	
		));
	}

	public function actionFaq()
	{
		$this->pageTitle = 'Faq - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['faq_meta_title'] != '')? $this->setting['faq_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['faq_meta_keyword'] != '')? $this->setting['faq_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['faq_meta_description'] != '')? $this->setting['faq_meta_description'] : $this->metaDesc;

		$this->render('faq', array(
			'model'=>$model,	
		));
	}

	public function actionLowongan()
	{
		$this->pageTitle = 'Lowongan - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['lowongan_meta_title'] != '')? $this->setting['lowongan_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['lowongan_meta_keyword'] != '')? $this->setting['lowongan_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['lowongan_meta_description'] != '')? $this->setting['lowongan_meta_description'] : $this->metaDesc;

		$criteria=new CDbCriteria;
        $criteria->addCondition('aktif = 1');
        $criteria->order = 't.tgl_input asc';
        $model = Lowongan::model()->findAll($criteria);

		$this->render('lowongan', array(
			'model'=>$model,	
		));
	}

	public function actionLowongan_detail()
	{
		$this->pageTitle = 'Lowongan - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->pageTitle = ($this->setting['lowongan_meta_title'] != '')? $this->setting['lowongan_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['lowongan_meta_keyword'] != '')? $this->setting['lowongan_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['lowongan_meta_description'] != '')? $this->setting['lowongan_meta_description'] : $this->metaDesc;

		$criteria=new CDbCriteria;
        $criteria->addCondition('t.id = :ids');
        $criteria->params[':ids'] = intval($_GET['id']);
        $criteria->addCondition('aktif = 1');
        $criteria->order = 't.tgl_input asc';
        $model = Lowongan::model()->find($criteria);

		$this->render('lowongan_detail', array(
			'model'=>$model,	
		));
	}

	public function actionContact()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Contact Us'.' - '.$this->pageTitle;

		$model = new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			$status = true;
	        $secret_key = "6LfiJdcUAAAAACDI6uev4S65Rm-EDM4ussOhnupd";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	        	$model->addError('email', Tt::t('front', 'You must complete the captcha first.'));
	        	$status = false;
	        }

			if ($status AND $model->validate()) {
				// $model->save();

				$mail = $this->renderPartial('//mail/contact', array(
					'model'=>$model,
				), true);

				$config = array(
					'to'=>array($this->setting['email'], $model->email),
					'bcc'=>array('info@jayamasinterior.com'),
					'subject'=>'Hello, New Contact medisku.com from '. $model->email,
					'message'=>$mail,
				);
				
				// echo "<pre>";
				// print_r($config);
				// echo "</pre>";
				// exit;
				
				// kirim email
				// Common::mail($config);

				Yii::app()->user->setFlash('success',Tt::t('front', 'Thank you for sending contacting form.'));
				$this->redirect(array('contact'));
			}
		}

		$this->pageTitle = ($this->setting['contact_meta_title'] != '')? $this->setting['contact_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['contact_meta_keyword'] != '')? $this->setting['contact_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['contact_meta_description'] != '')? $this->setting['contact_meta_description'] : $this->metaDesc;
		
		$this->render('contact', array(
			'model'=>$model,
		));
	}

	public function actionApplyregistration()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Daftar'.' - '.$this->pageTitle;

		$model = new TbMasterPerawat;
		if(isset($_POST['TbMasterPerawat']))
		{
			$model->attributes=$_POST['TbMasterPerawat'];

			$status = true;
	        // $secret_key = "6LfiJdcUAAAAACDI6uev4S65Rm-EDM4ussOhnupd";
	        // $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        // $response = json_decode($response);
	        // if($response->success==false)
	        // {
	        // 	$model->addError('email', Tt::t('front', 'You must complete the captcha first.'));
	        // 	$status = false;
	        // }

			$file_foto = CUploadedFile::getInstance($model,'file_foto');
			if ($file_foto->name != '') {
				$model->file_foto = substr(md5(time()),0,5).'-'.$file_foto->name;
			}
			$file_ktp = CUploadedFile::getInstance($model,'file_ktp');
			if ($file_ktp->name != '') {
				$model->file_ktp = substr(md5(time()),0,5).'-'.$file_ktp->name;
			}
			$file_ijazah = CUploadedFile::getInstance($model,'file_ijazah');
			if ($file_ijazah->name != '') {
				$model->file_ijazah = substr(md5(time()),0,5).'-'.$file_ijazah->name;
			}
			$file_sertifikat = CUploadedFile::getInstance($model,'file_sertifikat');
			if ($file_sertifikat->name != '') {
				$model->file_sertifikat = substr(md5(time()),0,5).'-'.$file_sertifikat->name;
			}
			$file_str = CUploadedFile::getInstance($model,'file_str');
			if ($file_str->name != '') {
				$model->file_str = substr(md5(time()),0,5).'-'.$file_str->name;
			}
			

			if ($status AND $model->validate()) {

				if ($file_foto->name != '') {
					$file_foto->saveAs(Yii::getPathOfAlias('webroot').'/images/mendaftar/'.$model->file_foto);
				}
				if ($file_ktp->name != '') {
					$file_ktp->saveAs(Yii::getPathOfAlias('webroot').'/images/mendaftar/'.$model->file_ktp);
				}
				if ($file_ijazah->name != '') {
					$file_ijazah->saveAs(Yii::getPathOfAlias('webroot').'/images/mendaftar/'.$model->file_ijazah);
				}
				if ($file_sertifikat->name != '') {
					$file_sertifikat->saveAs(Yii::getPathOfAlias('webroot').'/images/mendaftar/'.$model->file_sertifikat);
				}
				if ($file_str->name != '') {
					$file_str->saveAs(Yii::getPathOfAlias('webroot').'/images/mendaftar/'.$model->file_str);
				}
				
				$model->save(false);

				// $mail = $this->renderPartial('//mail/contact', array(
				// 	'model'=>$model,
				// ), true);

				// $config = array(
				// 	'to'=>array($this->setting['email'], $model->email),
				// 	'bcc'=>array('info@fotocopysurabaya.web.id'),
				// 	'subject'=>'Hello, New Contact medisku.com from '. $model->email,
				// 	'message'=>$mail,
				// );

				// echo "<pre>";
				// print_r($config);
				// echo "</pre>";
				// exit;
				
				// kirim email
				// Common::mail($config);

				Yii::app()->user->setFlash('success',Tt::t('front', 'Thank you for sent registration form.'));
				$this->redirect(array('applyregistration'));
			}
		}

		$this->pageTitle = ($this->setting['contact_meta_title'] != '')? $this->setting['contact_meta_title'] : $this->pageTitle;
		$this->metaKey = ($this->setting['contact_meta_keyword'] != '')? $this->setting['contact_meta_keyword'] : $this->metaKey;
		$this->metaDesc = ($this->setting['contact_meta_description'] != '')? $this->setting['contact_meta_description'] : $this->metaDesc;
		
		$this->render('register', array(
			'model'=>$model,
		));
	}


	public function actionEnquire()
	{
		$this->layout='//layouts/column2';
		$model = new Enquire;
		if(isset($_POST['Enquire']))
		{
			$model->attributes=$_POST['Enquire'];

			$status = true;
	        $secret_key = "6LeoESkUAAAAALvt2dGt0u_XsurphVpIWAwk8nnT";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	        	$model->addError('email', Tt::t('front', 'You must complete the captcha first.'));
	        	$status = false;
	        }

			if ($status AND $model->validate()) {
				// $model->save();

				$mail = $this->renderPartial('//mail/enquire', array(
					'model'=>$model,
				), true);

				$config = array(
					'to'=>array($this->setting['email'], $model->email),
					'bcc'=>array('info@fotocopysurabaya.web.id'),
					'subject'=>'Enquire medisku.com from '. $model->email,
					'message'=>$mail,
				);
				// echo "<pre>";
				// echo print_r($config);
				// echo "</pre>";
				// exit;

				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success',Tt::t('front', 'Thank you for sending contacting form.'));
				$this->redirect(array('contact'));
			}
		}
		exit;
	}


	public function actionError()
	{
		$this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				$this->layout='//layouts/column1';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;
				$this->render('error', array(
					'error'=>$error,
				));
			}
		}

	}

}