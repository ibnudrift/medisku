<?php

/**
 * This is the model class for table "tb_master_perawat".
 *
 * The followings are the available columns in table 'tb_master_perawat':
 * @property string $id
 * @property string $nama
 * @property string $email
 * @property string $phone
 * @property string $kelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $tinggi_badan
 * @property string $berat_badan
 * @property integer $anak_ke
 * @property integer $anak_dari
 * @property string $agama
 * @property string $riwayat_sakit
 * @property string $loc_alamat_lengkap
 * @property string $loc_provinsi
 * @property string $loc_kabupaten
 * @property string $loc_kecamatan
 * @property string $loc_kota
 * @property string $loc_kelurahan
 * @property string $pendidikan_terakhir
 * @property string $nama_sekolah
 * @property string $lulus_tahun
 * @property string $posisi_diminati
 * @property string $keahlian
 * @property string $pengalaman
 * @property string $gaji_diinginkan
 * @property string $nama_orangtua
 * @property string $kerja_orangtua
 * @property string $nama_suamiistri
 * @property string $pekerjaan_suamiistri
 * @property string $mengetahui_medisku
 * @property string $foto_wajah
 * @property string $foto_full
 * @property string $berkas_lamaran
 * @property string $pelatihan
 * @property string $sertifikat
 * @property integer $file_str
 * @property string $pengalaman_kerja
 * @property string $motivasi_bekerja
 * @property integer $bersedia_kontraktahun
 * @property integer $bersedia_standby
 * @property string $file_foto
 * @property string $file_ktp
 * @property string $file_ijazah
 * @property string $file_sertifikat
 * @property string $loc_alamat_domisili
 */
class MasterPerawat_test extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MasterPerawat_test the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_master_perawat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('anak_ke, anak_dari, file_str, bersedia_kontraktahun, bersedia_standby', 'numerical', 'integerOnly'=>true),
			array('nama, email, phone, kelamin, tempat_lahir, tinggi_badan, berat_badan, agama, loc_provinsi, loc_kabupaten, loc_kecamatan, loc_kota, loc_kelurahan, pendidikan_terakhir, nama_sekolah, lulus_tahun, posisi_diminati, keahlian, gaji_diinginkan, nama_orangtua, kerja_orangtua, nama_suamiistri, pekerjaan_suamiistri', 'length', 'max'=>100),
			array('foto_wajah, foto_full, berkas_lamaran, pelatihan, sertifikat, file_foto, file_ktp, file_ijazah, file_sertifikat', 'length', 'max'=>225),
			array('tanggal_lahir, riwayat_sakit, loc_alamat_lengkap, pengalaman, mengetahui_medisku, pengalaman_kerja, motivasi_bekerja, loc_alamat_domisili', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, email, phone, kelamin, tempat_lahir, tanggal_lahir, tinggi_badan, berat_badan, anak_ke, anak_dari, agama, riwayat_sakit, loc_alamat_lengkap, loc_provinsi, loc_kabupaten, loc_kecamatan, loc_kota, loc_kelurahan, pendidikan_terakhir, nama_sekolah, lulus_tahun, posisi_diminati, keahlian, pengalaman, gaji_diinginkan, nama_orangtua, kerja_orangtua, nama_suamiistri, pekerjaan_suamiistri, mengetahui_medisku, foto_wajah, foto_full, berkas_lamaran, pelatihan, sertifikat, file_str, pengalaman_kerja, motivasi_bekerja, bersedia_kontraktahun, bersedia_standby, file_foto, file_ktp, file_ijazah, file_sertifikat, loc_alamat_domisili', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'email' => 'Email',
			'phone' => 'Phone',
			'kelamin' => 'Kelamin',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'tinggi_badan' => 'Tinggi Badan',
			'berat_badan' => 'Berat Badan',
			'anak_ke' => 'Anak Ke',
			'anak_dari' => 'Anak Dari',
			'agama' => 'Agama',
			'riwayat_sakit' => 'Riwayat Sakit',
			'loc_alamat_lengkap' => 'Loc Alamat Lengkap',
			'loc_provinsi' => 'Loc Provinsi',
			'loc_kabupaten' => 'Loc Kabupaten',
			'loc_kecamatan' => 'Loc Kecamatan',
			'loc_kota' => 'Loc Kota',
			'loc_kelurahan' => 'Loc Kelurahan',
			'pendidikan_terakhir' => 'Pendidikan Terakhir',
			'nama_sekolah' => 'Nama Sekolah',
			'lulus_tahun' => 'Lulus Tahun',
			'posisi_diminati' => 'Posisi Diminati',
			'keahlian' => 'Keahlian',
			'pengalaman' => 'Pengalaman',
			'gaji_diinginkan' => 'Gaji Diinginkan',
			'nama_orangtua' => 'Nama Orangtua',
			'kerja_orangtua' => 'Kerja Orangtua',
			'nama_suamiistri' => 'Nama Suamiistri',
			'pekerjaan_suamiistri' => 'Pekerjaan Suamiistri',
			'mengetahui_medisku' => 'Mengetahui Medisku',
			'foto_wajah' => 'Foto Wajah',
			'foto_full' => 'Foto Full',
			'berkas_lamaran' => 'Berkas Lamaran',
			'pelatihan' => 'Pelatihan',
			'sertifikat' => 'Sertifikat',
			'file_str' => 'File Str',
			'pengalaman_kerja' => 'Pengalaman Kerja',
			'motivasi_bekerja' => 'Motivasi Bekerja',
			'bersedia_kontraktahun' => 'Bersedia Kontraktahun',
			'bersedia_standby' => 'Bersedia Standby',
			'file_foto' => 'File Foto',
			'file_ktp' => 'File Ktp',
			'file_ijazah' => 'File Ijazah',
			'file_sertifikat' => 'File Sertifikat',
			'loc_alamat_domisili' => 'Loc Alamat Domisili',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('kelamin',$this->kelamin,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('tinggi_badan',$this->tinggi_badan,true);
		$criteria->compare('berat_badan',$this->berat_badan,true);
		$criteria->compare('anak_ke',$this->anak_ke);
		$criteria->compare('anak_dari',$this->anak_dari);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('riwayat_sakit',$this->riwayat_sakit,true);
		$criteria->compare('loc_alamat_lengkap',$this->loc_alamat_lengkap,true);
		$criteria->compare('loc_provinsi',$this->loc_provinsi,true);
		$criteria->compare('loc_kabupaten',$this->loc_kabupaten,true);
		$criteria->compare('loc_kecamatan',$this->loc_kecamatan,true);
		$criteria->compare('loc_kota',$this->loc_kota,true);
		$criteria->compare('loc_kelurahan',$this->loc_kelurahan,true);
		$criteria->compare('pendidikan_terakhir',$this->pendidikan_terakhir,true);
		$criteria->compare('nama_sekolah',$this->nama_sekolah,true);
		$criteria->compare('lulus_tahun',$this->lulus_tahun,true);
		$criteria->compare('posisi_diminati',$this->posisi_diminati,true);
		$criteria->compare('keahlian',$this->keahlian,true);
		$criteria->compare('pengalaman',$this->pengalaman,true);
		$criteria->compare('gaji_diinginkan',$this->gaji_diinginkan,true);
		$criteria->compare('nama_orangtua',$this->nama_orangtua,true);
		$criteria->compare('kerja_orangtua',$this->kerja_orangtua,true);
		$criteria->compare('nama_suamiistri',$this->nama_suamiistri,true);
		$criteria->compare('pekerjaan_suamiistri',$this->pekerjaan_suamiistri,true);
		$criteria->compare('mengetahui_medisku',$this->mengetahui_medisku,true);
		$criteria->compare('foto_wajah',$this->foto_wajah,true);
		$criteria->compare('foto_full',$this->foto_full,true);
		$criteria->compare('berkas_lamaran',$this->berkas_lamaran,true);
		$criteria->compare('pelatihan',$this->pelatihan,true);
		$criteria->compare('sertifikat',$this->sertifikat,true);
		$criteria->compare('file_str',$this->file_str);
		$criteria->compare('pengalaman_kerja',$this->pengalaman_kerja,true);
		$criteria->compare('motivasi_bekerja',$this->motivasi_bekerja,true);
		$criteria->compare('bersedia_kontraktahun',$this->bersedia_kontraktahun);
		$criteria->compare('bersedia_standby',$this->bersedia_standby);
		$criteria->compare('file_foto',$this->file_foto,true);
		$criteria->compare('file_ktp',$this->file_ktp,true);
		$criteria->compare('file_ijazah',$this->file_ijazah,true);
		$criteria->compare('file_sertifikat',$this->file_sertifikat,true);
		$criteria->compare('loc_alamat_domisili',$this->loc_alamat_domisili,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}