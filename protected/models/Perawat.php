<?php

/**
 * This is the model class for table "tb_perawat".
 *
 * The followings are the available columns in table 'tb_perawat':
 * @property string $id
 * @property string $nama
 * @property string $image
 * @property string $jenis_kelamin
 * @property string $agama
 * @property string $pendidikan
 * @property string $tinggi_badan
 * @property string $berat_badan
 * @property string $sertifikat
 * @property string $pengalaman
 * @property string $penempatan
 * @property string $provinsi
 * @property string $kota
 * @property integer $tipe_perawat_id
 * @property integer $aktif
 * @property string $tgl_input
 * @property integer $pengalaman_lama
 */
class Perawat extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Perawat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_perawat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, jenis_kelamin', 'required'),
			array('aktif, pengalaman_lama', 'numerical', 'integerOnly'=>true),
			array('nama, penempatan, provinsi, kota', 'length', 'max'=>225),
			array('jenis_kelamin, agama, pendidikan, tinggi_badan, berat_badan', 'length', 'max'=>25),
			array('sertifikat, image, pengalaman, tgl_input, durasi_kontrak_max, tipe_perawat_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, image, jenis_kelamin, agama, pendidikan, tinggi_badan, berat_badan, sertifikat, pengalaman, penempatan, provinsi, kota, tipe_perawat_id, aktif, tgl_input, pengalaman_lama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'image' => 'Image',
			'jenis_kelamin' => 'Jenis Kelamin',
			'agama' => 'Agama',
			'pendidikan' => 'Pendidikan',
			'tinggi_badan' => 'Tinggi Badan',
			'berat_badan' => 'Berat Badan',
			'sertifikat' => 'Sertifikat',
			'pengalaman' => 'Pengalaman',
			'penempatan' => 'Penempatan',
			'provinsi' => 'Provinsi',
			'kota' => 'Kota',
			'tipe_perawat_id' => 'Tipe Perawat',
			'aktif' => 'Aktif',
			'tgl_input' => 'Tgl Input',
			'pengalaman_lama' => 'Pengalaman Lama',
			'durasi_kontrak_max' => 'Durasi Kontrak',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('tinggi_badan',$this->tinggi_badan,true);
		$criteria->compare('berat_badan',$this->berat_badan,true);
		$criteria->compare('sertifikat',$this->sertifikat,true);
		$criteria->compare('pengalaman',$this->pengalaman,true);
		$criteria->compare('penempatan',$this->penempatan,true);
		$criteria->compare('provinsi',$this->provinsi,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('tipe_perawat_id',$this->tipe_perawat_id);
		$criteria->compare('aktif',$this->aktif);
		$criteria->compare('tgl_input',$this->tgl_input,true);
		$criteria->compare('pengalaman_lama',$this->pengalaman_lama);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getsubject_durasi($name='')
	{
		$nmod_data = unserialize($name);

		$str = '';
		if (isset($nmod_data) && $nmod_data != '') {
			foreach ($nmod_data as $key => $value) {
				$str .= $value.', ';
			}
		}
		$str = substr($str, 0, -2);
		return $str;
	}

}